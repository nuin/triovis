class Distribution{
	int min, max, increment, arraySize;
    int[] countArray;
    int minCount, maxCount;
    int scale = 1;
    
    
    Distribution(int min,int  max,int increment) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.arraySize = ((max - min)/increment) +1;
        this.countArray = new int[arraySize];
    }

    
    void addCount(int vf) {
        //find the index, constrain to the array length
        int arrayIndex = round(constrain(map(vf, min, max, 0, arraySize-1), 0, arraySize-1));
        countArray[arrayIndex] ++;
    }
    
    String toString(){
        return Arrays.toString(countArray)+":max="+maxCount+" min="+minCount;
    }

    void findMaxAndMinCount() {
        int minC = Integer.MAX_VALUE;
        int maxC = Integer.MIN_VALUE;
        
        for(int i = 0; i < countArray.length; i++){
            int count = countArray[i];
            minC = (count < minC)?count:minC;
            maxC = (count > maxC)?count:maxC;
        }
        minCount = minC;
        maxCount = maxC;
    }



}
