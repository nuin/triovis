
// ControlP5 control;
Rectangle[] upBtns, downBtns;
Range[] varfreqRanges = new Range[3];
Slider[] coverageSliders = new Slider[3];

String[] controllerLabel = {"varfreq/zygosity", "coverage", "histogram"};
int[] controllerLabelX = new int[controllerLabel.length];

int _currentBtnSampleIndex = -1;

Range activeRange = null;
Slider activeSlider = null;

Cell _selectedCell = null;
ArrayList<Cell> selectedCells  = null;
HashMap<Integer, HashMap<Integer, Variant>> exportingResult = null; //<chrIndex, <position, Variant>>

String export_label = "export_VCF";
String[] migrating_label = {"with_migration", "without_migration"};
String optimisation_label ="optimise";
Rectangle[] text_btn_rects = new Rectangle[3];
boolean[] text_btn_mouseover ={false, false, false};


OptimisationThread optimisationThread = null;

boolean isOptimising = false;

//set up controller and their coordinates.
void setupController(){
	int runningY = varfreqY;

    upBtns = new Rectangle[3];
    downBtns = new Rectangle[3];

    int itemCounter = 0;
    for (int i = 0; i < _sampleLabels.length; i++) {
        if (i == 0) {
            controllerLabelX[itemCounter] = varfreqX;
        }
        Range varfreq = new Range(i, _VARFREQ_MIN, _VARFREQ_MAX, _defaultLowCutPoint, _defaultHighCutPoint, varfreqX, runningY, varfreqWidth , varfreqHeight);
        varfreq.update();
        
        itemCounter++;
       	varfreqRanges[i] = varfreq;
        
        if (i == 0) {
            controllerLabelX[itemCounter] = coverageX;
        }
        Slider coverage = new Slider(i, coverageSliderMin, coverageSliderMax, coverageSliderMin, coverageX, runningY, coverageWidth, coverageHeight);
        coverage.update();
        itemCounter++;
        coverageSliders[i] = coverage;

        upBtns[i] = new Rectangle(coverageX + coverageWidth, runningY, MARGIN, coverageHeight / 2);
        downBtns[i] = new Rectangle(coverageX + coverageWidth, runningY + coverageHeight / 2, MARGIN, coverageHeight / 2);

        //reset running values
        itemCounter = 0;
        runningY += varfreqHeight + controllerGapY;
    }
    //preictor
    controllerLabelX[2] = predictorX;

    //text buttons
    int runningX = varfreqX;
    int btn_y = _STAGE_HEIGHT - MARGIN*2;
    //export VCF
    textSize(12);
    textAlign(LEFT, TOP);
    int textWidth = round(textWidth(export_label));
    text_btn_rects[0] = new Rectangle(runningX,btn_y, textWidth, 15);
    runningX += textWidth + MARGIN*2;

    textWidth = round(textWidth(migrating_label[1]));
    text_btn_rects[1] = new Rectangle(runningX, btn_y, textWidth, 15);
    runningX += textWidth + MARGIN*2;
    //optimise btn
    textWidth = round(textWidth(optimisation_label));
    text_btn_rects[2] = new Rectangle(runningX, btn_y, textWidth, 15);


}
//***** Mouse *********//
void mousePressed(){
    //check hit area
    if(!fileChosen){
        //file not selected yet
        if(p1_btn.contains(mouseX, mouseY)){
            selectInput("Select a parent1 VCF file:", "parentOne");
        }else if(p2_btn.contains(mouseX, mouseY)){
            selectInput("Select a parent2 VCF file:", "parentTwo");
        }else if(c_btn.contains(mouseX, mouseY)){
            selectInput("Select a child/proband VCF file:", "child");
        }else if(go_btn.contains(mouseX, mouseY)){
            println("go button!");
            //check if all files
            boolean allFilesSelected = true;
            for(int i = 0; i<_files.length; i++){
                File f = _files[i];
                if(f == null){
                    allFilesSelected = false;
                    break;
                }
            }
            if(allFilesSelected){
                //load files
                fileChosen = true;
                startLoadingFiles();
            }else{
                println("not all files are selected");
                Runnable jThread = new FileChooseAlert();
                try{
                    SwingUtilities.invokeAndWait(jThread);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }else{ //everything else
        if(!isLoading){
            if(optimisationThread != null ){
                if(!optimisationThread.doneOnce){
                    //do nothing what so ever
                    println("ignore input");
                    return;
                }
            }
            println(" did it really ignore?");
            //y range
            if(mouseY > plotY1 && mouseY <plotY2){
                //cell area
                //check if it hits a cell
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j< 3; j++){
                        for(int k = 0; k < 3; k++){
                            Cell cell = data.table[i][j][k];
                            if(cell.rect.contains(mouseX, mouseY)){
                                println("celll click:"+i+" "+j+" "+k);
                                cell.isSelected =  !cell.isSelected;
                                return;
                            }
                        }
                    }
                }
            }else if(mouseY > varfreqY && mouseY < varfreqY+predictorHeight){
                if(mouseX > varfreqX - MARGIN && mouseX < varfreqX+varfreqWidth){
                    //varfreq 
                    for(int i = 0; i < varfreqRanges.length; i++){
                        Range r = varfreqRanges[i];
                        //specify which handle is selected
                        if(r.h_low.contains(mouseX, mouseY)){
                            activeRange = r;
                            activeRange.isLowSelected = true; 
                            return;
                        }else if(r.h_high.contains(mouseX, mouseY)){
                            activeRange = r;
                            activeRange.isHighSelected = true;
                            return;
                        }
                    }
                }else if( mouseX > coverageX -MARGIN && mouseX < coverageX+coverageWidth){
                    //coverage
                    for(int i = 0; i < coverageSliders.length; i++){
                        Slider r = coverageSliders[i];
                        if(r.handle_rect.contains(mouseX, mouseY)){
                            activeSlider = r;
                            _currentBtnSampleIndex = i;
                            return;
                        }
                    }
                }else if( mouseX >coverageX+coverageWidth && mouseX < coverageX+coverageWidth+MARGIN){
                    //up or down buttons
                    for (int i = 0; i < upBtns.length; i++) {
                        if (upBtns[i].contains(mouseX, mouseY)) {
                            if(!isUpdating){ //only when updating is done
                                // println("debug: upBtn click");
                                Slider slider = coverageSliders[i];
                                slider.value_current = min(slider.value_current+1, _COVERAGE_MAX);
                                slider.update();
                                data.updateCoverage(data.samples[slider.sampleIndex], slider.value_current);
                                predictor.updateAll();
                                loop();
                            }
                            return;
                        } else if (downBtns[i].contains(mouseX, mouseY)) {
                            if(!isUpdating){
                                Slider slider = coverageSliders[i];
                                slider.value_current = max(slider.value_current-1, _COVERAGE_MIN);
                                slider.update();
                                data.updateCoverage(data.samples[slider.sampleIndex], slider.value_current);
                                predictor.updateAll();
                                loop();
                            }
                            return;
                        }
                    }
                }
            }else if(mouseY > text_btn_rects[0].y && mouseY < text_btn_rects[0].y+text_btn_rects[0].height){
                if(text_btn_rects[0].contains(mouseX, mouseY)){
                    //eport vcf
                    println("export filtered VCF");
                    if(isLoading){
                        println("still loading....");
                    }else{
                        getReadyForExport();
                        exportVCF();
                    }
                }else if(text_btn_rects[1].contains(mouseX, mouseY)){
                    //change migrating mode
                    println("migrate:"+_MIGRATING);
                    //update current status
                    hardReset(); //reset current coverage
                    _MIGRATING = !_MIGRATING;
                }else if(text_btn_rects[2].contains(mouseX, mouseY)){
                    //optimise
                    println("Optimise!");
                    isOptimising = true;
                    hardReset();
                    if(optimisationThread == null){
                        optimisationThread  = new OptimisationThread(1,10);
                        new Thread(optimisationThread).start();

                    }else{
                        //done
                        optimisationThread.setOptimal();
                    }

                }

            }     
            
        }
    }
}
void mouseMoved() {
    //hovering action
    if(optimisationThread != null){
        if(!optimisationThread.doneOnce){
            //do nothing what so ever
            return;
        }
    }

    if (!isLoading && !mousePressed) { 
        if (mouseY > plotY1 && mouseY < plotY2 && mouseX > plotX1 && mouseX < plotX2) {
            cursor(HAND);
            loop();
        }else if(mouseX > varfreqX && mouseX < coverageX+coverageWidth+MARGIN && mouseY >varfreqY && mouseY < (varfreqY+predictorHeight)){
            loop();
        }else if(predictorRect.contains(mouseX, mouseY)){
            //check if it is hitting histogram_rects
            if(histogram_rects!=null){
                for(int i = 0; i<histogram_rects.length; i++){
                    Rectangle rect = histogram_rects[i];
                    if(rect.contains(mouseX, mouseY)){
                        selected_bar = i;
                        loop();
                        return;
                    }
                }
                selected_bar = -1;
                loop();
            }else{
                selected_bar = -1;
            }
        }else if(mouseY > text_btn_rects[0].y && mouseY < text_btn_rects[0].y+text_btn_rects[0].height){
            //check which button
            if(mouseX > text_btn_rects[0].x && mouseX < text_btn_rects[0].x +text_btn_rects[0].width){
                //export
                text_btn_mouseover[0]= true;
                text_btn_mouseover[1] = false;
                text_btn_mouseover[2] = false;
                cursor(HAND);
            }else if(mouseX > text_btn_rects[1].x && mouseX < text_btn_rects[1].x +text_btn_rects[1].width){
                text_btn_mouseover[0]= false;
                text_btn_mouseover[1] = true;
                text_btn_mouseover[2] = false;
                cursor(HAND);
            }else if(mouseX > text_btn_rects[2].x && mouseX < text_btn_rects[2].x +text_btn_rects[2].width){
                text_btn_mouseover[0]= false;
                text_btn_mouseover[1] = false; 
                text_btn_mouseover[2] = true;
                cursor(HAND);
            }else{
                text_btn_mouseover[0]= false;
                text_btn_mouseover[1] = false; 
                text_btn_mouseover[2] = false;
                cursor(ARROW);
            }
            loop();
            return;

        }else{
            cursor(ARROW);
            selected_bar = -1;
            text_btn_mouseover[0]= false;
            text_btn_mouseover[1] = false; 
            text_btn_mouseover[2] = false;
            loop();
            return;
        }
    }
}

void mouseDragged() {
    if(activeRange != null){
        varfreqInteraction();
    }else if( activeSlider != null){
        coverageInteraction();        
    }
}

void mouseReleased() { 
    activeSlider = null;
    if(activeRange != null){
        activeRange.isLowSelected = false;
        activeRange.isHighSelected = false;
    }
    activeRange = null;
    _currentBtnSampleIndex = -1;

    loop();
}

//called when varfreq value is changed
void varfreqInteraction() { ///// reimplement this
    int sampleIndex = activeRange.sampleIndex;
    Sample s = data.samples[sampleIndex];
    activeRange.updateValue(mouseX, mouseY);
    int filterMin = activeRange.value_current_low;
    int filterMax = activeRange.value_current_high;
    data.updateVarfreq(s, filterMin, filterMax);
    // predictor.updateAll();
    loop();
}
//called when coverage slider is changed on drag
void coverageInteraction() {
    int sampleIndex = activeSlider.sampleIndex;
    Sample s = data.samples[sampleIndex];
    activeSlider.updateValue(mouseX);
    int filterMin = activeSlider.value_current;
    data.updateCoverage(s, filterMin);

    // predictor.updateAll();
    loop();
}


//******* Keyboard ******* //
void keyPressed() {
    if (key == 'p') {
        println("print");
        this.saveFrame = true;
        loop();
    } else if (key == 'e') {
        println("export filtered VCF");
        if(isLoading){
            println("still loading....");
        }else{
            getReadyForExport();
            exportVCF();
        }
    } else if (key == '2') {
    } else if (keyCode == ENTER) {
        System.out.println("Enter hit!");
    } else if( key == 'm'){
        println("migrate:"+_MIGRATING);
        //update current status
        hardReset(); //reset current coverage
        _MIGRATING = !_MIGRATING;
        
    }
}

void hardReset(){
    for(int i = 0; i < data.samples.length; i++){
        Sample s = data.samples[i];
        s.currentCoverage = 1;
        // data.updateCoverage(s, 1);
        Slider slider = coverageSliders[i];
        slider.value_current =1;
        slider.update();
        // predictor.updateAll();
        // loop();
    }
    isLoading = true;
    loop();
    initialize();
}


//select a cell or cells to export the variants
void getReadyForExport(){
    //check if a cell / cells selected
    selectedCells = new ArrayList<Cell>();
    for(int i = 0; i< data.table.length; i++){
        for(int j = 0; j<data.table.length; j++){
            for(int k = 0; k<data.table.length; k++){
                Cell cell = data.table[i][j][k];
                if(cell.isSelected){
                    selectedCells.add(cell);
                    println("selected cell:"+Arrays.toString(cell.indexes));
                }
            }
        }
    }

    int totalPossible = globalInitialCount[0];
    int totalImpossible = globalInitialCount[1];
    int selectedPossible = 0;
    int selectedImpossible = 0;



    //if no cell selected
    if(selectedCells.isEmpty()){
        println("no cell selected yet");
        // int selectedValue = JOptionPane.showConfirmDialog(null, "Would you like to export all the variants?", "Export Filtered VCF", JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
        // if(selectedValue == JOptionPane.YES_OPTION){
        for(int i = 0; i< data.table.length; i++){
            for(int j = 0; j<data.table.length; j++){
                for(int k = 0; k<data.table.length; k++){
                    Cell cell = data.table[i][j][k];
                    if(i == 0 && j == 0 && k ==0){
                    }else{
                        selectedCells.add(cell);                        
                    }
                }
            }
        }
        println("selecetd cell count ="+selectedCells.size());
        exportingResult = new HashMap<Integer, HashMap<Integer, Variant>>(); 
        //iterate through selected cell
        for(int i = 0; i<selectedCells.size(); i++){
            Cell cell = selectedCells.get(i);
            boolean isPossible = _combination[cell.indexes[0]][cell.indexes[1]][cell.indexes[2]];
            //iterate through variants
            Iterator ite = cell.variants.iterator();
            while(ite.hasNext()){
                Variant v = (Variant)ite.next();  
                HashMap<Integer, Variant> chrArray = (HashMap<Integer, Variant>)exportingResult.get(v.chrIndex);
                if(chrArray == null){
                    chrArray = new HashMap<Integer, Variant>();
                    exportingResult.put(v.chrIndex, chrArray);
                }
                //record the variant position
                chrArray.put(v.position, v);
                if(isPossible){
                    selectedPossible ++;
                }else{
                    selectedImpossible++;
                }
            }
        }
        // }else if(selectedValue == JOptionPane.NO_OPTION){
        //     exportingResult = null;
        // }

    }else{
        println("selecetd cell count ="+selectedCells.size());
        exportingResult = new HashMap<Integer, HashMap<Integer, Variant>>(); 
        //iterate through selected cell
        for(int i = 0; i<selectedCells.size(); i++){
            Cell cell = selectedCells.get(i);
            boolean isPossible = _combination[cell.indexes[0]][cell.indexes[1]][cell.indexes[2]];
            //iterate through variants
            Iterator ite = cell.variants.iterator();
            while(ite.hasNext()){
                Variant v = (Variant)ite.next();            
                HashMap<Integer, Variant> chrArray = (HashMap<Integer, Variant>)exportingResult.get(v.chrIndex);
                if(chrArray == null){
                    chrArray = new HashMap<Integer, Variant>();
                    exportingResult.put(v.chrIndex, chrArray);
                }
                //record the variant position
                chrArray.put(v.position, v);
                if(isPossible){
                    selectedPossible ++;
                }else{
                    selectedImpossible++;
                }
            }
        }
    }

    int possiblePercent = round((float)selectedPossible/(float)totalPossible*100);
    int impossiblePercent = round((float)selectedImpossible/(float)totalImpossible *100);
    
    String msg = "Exporting variants from "+selectedCells.size()+" blocks:\n\t\t"+selectedPossible+" possible variants ("+possiblePercent+"%)\n\t\t"+selectedImpossible+" impossible variants ("+impossiblePercent+"%)";
    int exportOrNot = JOptionPane.showConfirmDialog(null, msg, "Export Filtered VCF", JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);
    if(exportOrNot == JOptionPane.OK_OPTION){
    }else if(exportOrNot == JOptionPane.CANCEL_OPTION){
        exportingResult = null;
    }
}

void exportVCF(){
    //run on another thread
    if(exportingResult != null){
        isWritingFile = true;
        Runnable readAndWrite  = new ReadAndWriteThread();
        new Thread(readAndWrite).start();
        
    }
}
//method called from ReadAndWriteThread
void readAndWrite(String path, String p){
    //create writer
    String[] fileName = splitTokens(path, ".");
    PrintWriter writer = createWriter(p+fileName[0]+"_filtered.vcf");
    Sample s = data.samples[loadingSampleIndex];
    int s_coverage = s.currentCoverage;
    File f = new File(dataPath(path));
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(f);
        //Loop to read the file one line at a time
        String line = null;
        while ((line = reader.readLine()) != null) {
            if(line.startsWith("##")){
                //attribute
                writer.println(line);
            }else if(line.startsWith("#")){
                //header
                writer.println(line);
            }else{
                String[] st = split(line, TAB);
                if (st[0] != null) {
                    String chr = st[0].trim();
                    int chrIndex = data.getChrIndex(chr);
                    if(chrIndex == -1){
                        print("Error:");
                    }
                    int position = Integer.parseInt(st[1]);
                    HashMap<Integer, Variant> map = (HashMap<Integer, Variant>)exportingResult.get(chrIndex);
                    if(map == null){
                        println("cannot find chromosome id:"+chr);
                    }else{
                        Variant v = (Variant)map.get(position);
                        if( v != null){
                            //find Row
                            Row r = v.getRow(loadingSampleIndex);
                            if(r != null){
                                // find coverage
                                if(r.coverage >= s_coverage){
                                    //write
                                    writer.println(line);
                                }else{
                                    //don't wirte
                                }
                            }else{
                                println("Error: Row is null. Controller.readAndWrite()");
                            }
                        }else{
                            //this variant was not selected
                        }
                    }
                }
            }
            writer.flush();
        }
    } catch (IOException e) {
        e.printStackTrace();
    }

    writer.close();
    println("finnished writing: "+fileName[0]+"_filtered.vcf");
}
//real mode
void readAndWrite(File file, String p){
    //create writer
    String[] fileName = splitTokens(file.getName(), ".");
    PrintWriter writer = createWriter(p+fileName[0]+"_filtered.vcf");//createWriter(sketchPath+"/data/"+fileName[0]+"_filtered.vcf");
    Sample s = data.samples[loadingSampleIndex];
    int s_coverage = s.currentCoverage;

    File f = file;
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(f);
        //Loop to read the file one line at a time
        String line = null;
        while ((line = reader.readLine()) != null) {
            if(line.startsWith("##")){
                //attribute
                writer.println(line);
            }else if(line.startsWith("#")){
                //header
                writer.println(line);
            }else{
                String[] st = split(line, TAB);
                if (st[0] != null) {
                    String chr = st[0].trim();
                    int chrIndex = data.getChrIndex(chr);
                    if(chrIndex == -1){
                        print("Error:");
                    }
                    int position = Integer.parseInt(st[1]);

                    HashMap<Integer, Variant> map = (HashMap<Integer, Variant>)exportingResult.get(chrIndex);
                    if(map == null){
                        println("cannot find chromosome id:"+chr);
                    }else{
                        Variant v = (Variant)map.get(position);
                        if( v != null){
                            //find Row
                            Row r = v.getRow(loadingSampleIndex);
                            if(r != null){
                                // find coverage
                                if(r.coverage >= s_coverage){
                                    //write
                                    writer.println(line);
                                }else{
                                    //don't wirte
                                }

                            }else{
                                println("Error: Row is null. Controller.readAndWrite()");
                            }
                        }
                    }
                }
            }
            writer.flush();
        }
    } catch (IOException e) {
        e.printStackTrace();
    }

    writer.close();
    println("finnished writing: "+fileName[0]+"_filtered.vcf");
}





















