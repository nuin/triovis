class PredictorThread implements Runnable{
    int cMin = 1;
    int cMax = 21;
    int countMin = 0;
    int countMax = 2000;  // hight max of predictor
    int binSize = cMax - cMin;
    int[][][] counters = new int[3][2][binSize]; // sample, possibleOrImpossible,coverage-1
    boolean running = true;
    int realCountMax = -1;
    int countInterval = 100;

    int[][][][][] variant_counters = new int[3][binSize][3][3][3]; //sampleIndex, coverage -1, cell indexes

    void run() {
        // println("PredictorThread.run()");
        if(running){
            //do this..
            for(int i = 0; i < 3; i++){
                updateCounter(i);
            }
            running = false;
        }    
        //find counterMax
        if(realCountMax == -1){
            //not defined yet
            for(int i = 0; i < counters.length; i++){
                for(int j = 0; j < counters[0].length; j++){
                    for(int k = 0; k < counters[0][0].length; k++){
                        realCountMax = max(counters[i][j][k], realCountMax);
                    }
                }
            }
            //scalkng the display size
            //adjust interval size
            if(realCountMax > 10000){
                countInterval = 1000;
            }
            //adjust the size of max y on predictor
            countMax = ((realCountMax/countInterval)+1)*countInterval;
            println("debug: PredictorThread: real count max = "+realCountMax +"   countMax = "+countMax);
        }

        loop();
    }
    // i is the sample index
    void updateCounter(int i){
        // println("predictorThread updateCounter()");
        // Sample s = data.samples[_sampleOrder[i]];
        counters[i] = predict(_sampleOrder[i]);
    }
    void updateAll(){
        // println("PredictorThread.updateAll()");
        running = true;
        run();
    }
     //calculate predictor change
    int[][] predict(int sampleIndex) {
        variant_counters[sampleIndex] = new int[binSize][3][3][3]; //number of variants
        //0=possible  1=impossible
        int[][] result = new int[2][binSize];
        Sample selected = data.samples[sampleIndex];
        //get array of ArrayList
        ArrayList<Row>[] allRows = selected.getRows(cMin, cMax); //****************shoud be hashset
        for(int i = 0; i< allRows.length; i++){
            //iterate per coverage level, by going through arraylist
            ArrayList<Row> rows = allRows[i];
            if(rows != null){
                for(int j = 0; j<rows.size(); j++){
                    Row r = rows.get(j);
                    Variant v = r.parentVariant;
                    int rowCoverage = r.coverage;
                     //if _MIGRATING
                     if(_MIGRATING){
                        //check its new cell index and see if the variant is possible
                        int[] indexes = v.cellIndex;
                        if(indexes != null ){
                            if(indexes[0]==0 && indexes[1]==0 && indexes[2]==0){
                                //regard it as no call, all ref/ref
                            }else{
                                boolean possible = _combination[indexes[0]][indexes[1]][indexes[2]];
                                if(possible){
                                    result[0][i]++;
                                }else{
                                    result[1][i]++;
                                }
                                //count   
                                variant_counters[sampleIndex][rowCoverage-1][indexes[0]][indexes[1]][indexes[2]]++;

                            }
                        }
                     }else{
                        //check the samples coverage
                        int coverage = selected.currentCoverage;
                        //if it is above the coverage, add count
                        if(rowCoverage >= coverage){
                            int[] indexes = v.cellIndex;
                            if(indexes != null ){
                                if(indexes[0]==0 && indexes[1]==0 && indexes[2]==0){
                                    //regard it as no call, all ref/ref
                                }else{
                                    boolean possible = _combination[indexes[0]][indexes[1]][indexes[2]];
                                    if(possible){
                                        result[0][i]++;
                                    }else{
                                        result[1][i]++;
                                    }
                                    //count   
                                    variant_counters[sampleIndex][rowCoverage-1][indexes[0]][indexes[1]][indexes[2]]++;

                                }
                            }   
                        }

                     }   
                    // }
                } 
            }
        }  
        return result;
    }

}
