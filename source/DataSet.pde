String[] _sampleLabels = new String[3]; //sample ID
String[] _chrLabels = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20","chr21", "chr22", "chrX", "chrY"};
boolean[][][] _combination = {{{true, false, false}, {true, true, false}, {false, true, false}},
        {{true, true, false}, {true, true, true}, {false, true, true}},
        {{false, true, false}, {false, true, true}, {false, false, true}}};
String[][][] _cellLable = {{{"", "de novo", ""}, {"", "", ""}, {"", "", ""}},
        {{"", "", ""}, {"", "", "recessive"}, {"", "", ""}},
        {{"", "", ""}, {"", "", ""}, {"", "", ""}}};
int[] _sampleOrder = {0, 1, 2}; //initial default order
//varfreq range
int _VARFREQ_MIN = 0;
int _VARFREQ_MAX = 100;
//coverage range
int _COVERAGE_MIN = 1; 
int _COVERAGE_MAX = 200;
int _defaultLowCutPoint = 20;
int _defaultHighCutPoint = 90;

// int[] sampleMinCoverages = {1, 1, 1};

class DataSet{
	Sample[] samples; //all samples
	HashMap<Integer, HashMap> variantChrMap; //variants sorted by chromosomes
	HashSet<Variant> allVariants;
	HashSet<Variant> excludedVariants; //variants below the varfreq thresholds
	Cell[][][] table; //table of cells


	DataSet(){
		samples = new Sample[3];
		allVariants = new HashSet<Variant>();
		variantChrMap = new HashMap<Integer, HashMap>();
	}

	//get index of chr from the name
	int getChrIndex(String chr){
        if(!chr.startsWith("chr")){
            chr = "chr"+chr;
        }
        for(int i = 0; i< _chrLabels.length; i++){
            if(chr.equals(_chrLabels[i])){
                return i;
            }
        }
        return -1;//no match
    }
    //returns Sample object, if not exist, creates a new Sample
    Sample findSample(int sampleIndex) {
    	Sample s = samples[sampleIndex];
    	if(s == null){
    		s = new Sample(_sampleLabels[sampleIndex]);
    		samples[sampleIndex]= s;
    	}
    	return s;
    }
    //returns Variant object, if not exist, creates a new Variant
    Variant findVariant(int chr, int position, String id){     
        //locate the chromosome
        HashMap chrMap = variantChrMap.get(chr);
        if(chrMap == null){
            chrMap = new HashMap<Integer, Variant>();
            variantChrMap.put(chr, chrMap);
        }
        //find the variant
        Variant v = (Variant) chrMap.get(position);
        if(v == null){
            v = new Variant(chr, position, id);
            chrMap.put(position, v);
            allVariants.add(v);
        }
        return v;
    }
    //setup the zygosity of each Row, called only once
    void setupZygosity() {
        for (int i = 0; i < samples.length; i++) {
            Sample s = samples[i];
            s.setupZygosity();
        }
    }
    //setup table, called only once at the beginning
    void setupTable() {
        //setup table
        table = new Cell[3][3][3];
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                for (int k = 0; k < table.length; k++) {
                    table[i][j][k] = new Cell(i, j, k);
                }
            }
        }
        //excluded variant
        excludedVariants = new HashSet<Variant>();
        //loader
        currentLoaded = 0;
        fileSize = allVariants.size();
        Iterator ite = allVariants.iterator();
        while(ite.hasNext()){
            Variant v = (Variant)ite.next();

        // for (int i = 0; i < allVariants.size(); i++) {
        // Variant v = allVariants.get(i);
            int[] index = setupVariantIndex(v); //called only once
            if(index == null){
                //exclude the variant
                //one of row has out of range varfreq
                excludedVariants.add(v);
            }else{
                //add the variant to the cell
                table[index[0]][index[1]][index[2]].addVariant(v);
            }
            currentLoaded ++;
        }
        println("debug:DataSet.setupTable():excluded vairant based on varfreq threshold =" + excludedVariants.size());
    }


    //find the variant index, the very first time
    //returns null if the variant is excluded due to varfreq below low boundary
    //it assumes that if there was no call, it becomes Ref/Ref
    int[] setupVariantIndex(Variant v) {
        int[] result = {0, 0, 0};
        ArrayList<Row> rows = v.rows;
        for (int i = 0; i < rows.size(); i++) {
            Row r = rows.get(i);
            int sampleIndex = r.sampleIndex;
            int sampleOrderIndex = _sampleOrder[sampleIndex];
            if (r.isRefAlt) {
                result[sampleOrderIndex] = 1;
            } else if (r.isAltAlt) {
                result[sampleOrderIndex] = 2;
            } else {
                //varfreq below 20
                return null;
            }
        } 
        //debug
        if (rows.size() > 3) {
            println("Error: setupVariantIndex " + rows.size() + " v-id=" + v.variantID);
        }
        return result;
    }

    //find the variant index //only _MIGRATING mode
    //returns null if the variant is excluded due to varfreq below low boundary
    int[] getVariantIndex(Variant v) {
        int[] result = {0, 0, 0}; // 
        ArrayList<Row> rows = v.rows;
        for (int i = 0; i < rows.size(); i++) {
            Row r = rows.get(i);
            int sampleIndex = r.sampleIndex;
            int sampleOrderIndex = _sampleOrder[sampleIndex];
            Sample sample = data.samples[sampleIndex];
            int coverage = r.coverage;
            int currentCoverage = sample.currentCoverage;
            if (r.isRefAlt) {
                if(coverage >=currentCoverage){
                    result[sampleOrderIndex] = 1;
                }else{
                    //consider it Ref/Ref
                    result[sampleOrderIndex] = 0;
                }
            } else if (r.isAltAlt) {
                if(coverage >=currentCoverage){
                    result[sampleOrderIndex] = 2;
                }else{
                    result[sampleOrderIndex] = 0;    
                }
            } else {
                //below varfreq threashold
                return null;   
            }
        } 
        //debug
        if (rows.size() > 3) {
            println("Error DataSet.getVariantIndex() " + rows.size() + " v-id=" + v.variantID);
        }
        return result;
    }

    //calculate distribution for each sample
    void calculateVarfreqDistributions() {
        //varfreq 
        for (int j = 0; j < samples.length; j++) {
            Sample s = samples[j];
            Distribution d = new Distribution(_VARFREQ_MIN, _VARFREQ_MAX, 5);  //5 value increment 
            ArrayList<Row> rows = s.rows;
            for (int i = 0; i < rows.size(); i++) {
                Row r = rows.get(i);
                int vf = r.varfreq;
                d.addCount(vf);
            }
            d.findMaxAndMinCount();
            s.varfreqDistribution = d;
            // println(s.sampleID+": varfreq distribution:"+d.toString());
        }
    }
    //calculate coverage distribution for each sample
    void calculateCoverageDistributions() {
        //coverage
        for (int j = 0; j < samples.length; j++) {
            Sample s = samples[j];
            Distribution d = new Distribution(_COVERAGE_MIN, _COVERAGE_MAX, 10);
            ArrayList<Row> rows = s.rows;
            for (int i = 0; i < rows.size(); i++) {
                Row r = rows.get(i);
                int cov = r.coverage;
                d.addCount(cov);
            }
            d.findMaxAndMinCount();
            s.coverageDistribution = d;
        }
    }


    //update based on the varfreq ranges
    void updateVarfreq(Sample s, int filterMin, int filterMax) {
        ArrayList<Variant> variantsToUpdate = s.updateVarfreqRange(filterMin, filterMax);
        //move around variant
        for(int i = 0; i<variantsToUpdate.size(); i++){
            Variant v = variantsToUpdate.get(i);
            int[] oldIndex = v.cellIndex;
            int[] newIndex = getVariantIndex(v);
            
            if(oldIndex == null){
                //previously excluded variants
                if(excludedVariants.contains(v)){
                    if(newIndex == null){
                        //at least one of rows is still below the minimum varfreq
                        v.cellIndex = null;
                    }else{
                        excludedVariants.remove(v);
                        table[newIndex[0]][newIndex[1]][newIndex[2]].addVariant(v);                    
                    }
                }else{
                    println("debug: error in updateVarfreq(): not in excluded:"+v.toString());
                }
            }else{
                if(excludedVariants.contains(v)){
                    if(newIndex == null){
                        //still at least one of rows is still below minimum varfreq
                        v.cellIndex = null;
                    }else{
                        excludedVariants.remove(v);
                        table[newIndex[0]][newIndex[1]][newIndex[2]].addVariant(v);    
                    }
                }else{
                    //remove variant from old cell
                    table[oldIndex[0]][oldIndex[1]][oldIndex[2]].removeVariant(v);
                    if(newIndex  == null){
                        excludedVariants.add(v);
                        v.cellIndex = null;
                    }else{
                        //add variant to new cell
                        table[newIndex[0]][newIndex[1]][newIndex[2]].addVariant(v);
                    }
                }   
            }   
        }    
        predictor.updateAll();
    }
    //update based on the coverage, called when overage changes
    void updateCoverage(Sample s, int filterMin) {
        // println("DataSet.updateCoverage()");
        int prevCoverage = s.currentCoverage;
        s.currentCoverage = filterMin;
        //whether to remove or add these variants
        boolean increasing = (prevCoverage < filterMin)?true:false;
        
        //set the minmum filter value for this sample
        //ArrayList<Variant> vs = s.updateCoverage(filterMin, prevCoverage); //list of variants to update
        ConcurrentHashMap<String, Variant> vs = s.updateCoverage2(filterMin, prevCoverage);
        // println("debug: number of variants to update= "+vs.size());

        Thread updator = new UpdateThread(vs, increasing);
        updator.start();

        //update predictor
        predictor.updateAll();
    }

    void updateCoverageNonThread(Sample s, int filterMin){
        int prevCoverage = s.currentCoverage;
        s.currentCoverage = filterMin;
        //whether to remove or add these variants
        boolean increasing = (prevCoverage < filterMin)?true:false;
        //set the minmum filter value for this sample
        ConcurrentHashMap<String, Variant> vs = s.updateCoverage2(filterMin, prevCoverage);
        Iterator ite = vs.entrySet().iterator();
        while(ite.hasNext()){
            Map.Entry entry = (Map.Entry)ite.next();
            //action
            // count++;
            Variant v = (Variant) entry.getValue();
            if(_MIGRATING){
                int[] newIndex = data.getVariantIndex(v);
                int[] oldIndex = v.cellIndex;
                if(oldIndex == null){
                    //variants outside of the varfreq range
                }else{
                    //remove variant from the old cell
                    Cell cell = data.table[oldIndex[0]][oldIndex[1]][oldIndex[2]];
                    cell.removeVariant(v);
                    //add variant to the new cell
                    cell = data.table[newIndex[0]][newIndex[1]][newIndex[2]];
                    cell.addVariant(v);
                }
            }else{
                int[] cellIndex  = v.cellIndex;
                if(cellIndex == null){
                    //variants outside of the Varfreq Reange
                }else{
                    Cell cell = data.table[cellIndex[0]][cellIndex[1]][cellIndex[2]];
                    if(increasing){
                        cell.removeVariant(v);
                    }else{
                        cell.addVariant(v);
                    }
                }
            }     
        }
    }
    //count how many possible and imposibble variatns
    int[] getGlobalCounts() {
        int[] result = new int[2];  //0 blue, 1 red
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                for (int k = 0; k < this.table.length; k++) {
                    if (i == 0 && j == 0 && k == 0) {
                        //do not add to global count
                    } else {
                        Cell cell = table[i][j][k];
                        if (_combination[i][j][k]) {
                            //possible
                            result[0] += cell.variants.size();
                        } else {
                            //impossible
                            result[1] += cell.variants.size();
                        }                        
                    }
                }
            }
        }
        return result;
    }


}
