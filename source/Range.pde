class Range{
	int sampleIndex;
	int handleSize = 2;
	int value_min, value_max, value_current_low, value_current_high;
	Rectangle display_rect;

	int dx_low, dx_high;
	Rectangle h_low, h_high;
	boolean isLowSelected = false;
	boolean isHighSelected = false;

	Range(int n, int min, int max, int def_l,int def_h, int dx, int dy, int dw, int dh){
		sampleIndex = n;
		value_min = min;
		value_max = max;
		value_current_low = def_l;
		value_current_high = def_h;
		display_rect = new Rectangle(dx, dy, dw, dh);
	}


	void update(){
		dx_low= round(map(value_current_low, value_min, value_max, display_rect.x, display_rect.x+display_rect.width));
		dx_high= round(map(value_current_high, value_min, value_max, display_rect.x, display_rect.x+display_rect.width));

		h_low = new Rectangle(dx_low-handleSize, display_rect.y, handleSize*2,display_rect.height);
		h_high = new Rectangle(dx_high-handleSize, display_rect.y, handleSize*2,display_rect.height);

		// println("display rect="+display_rect);
		// println("handle rect="+handle_rect);

	}

	void updateValue(int mx, int my){
		if(isLowSelected){
			//low varfreq change
			//update display value
			dx_low = constrain(mx, display_rect.x, dx_high-handleSize); //never go pass the high value
			h_low = new Rectangle(dx_low-handleSize, display_rect.y, handleSize*2,display_rect.height);
			value_current_low = round(map(dx_low, display_rect.x, display_rect.x+display_rect.width, value_min, value_max));
		}else if(isHighSelected){
			//high varfreq change
			dx_high = constrain(mx, dx_low+handleSize, display_rect.x+display_rect.width);
			h_high = new Rectangle(dx_high-handleSize, display_rect.y, handleSize*2,display_rect.height);
			value_current_high = round(map(dx_high, display_rect.x, display_rect.x+display_rect.width, value_min, value_max));
		}else{
			println("Error:neither handle selected");
		}
	}

}
