class ReadAndWriteThread implements Runnable{
	void run(){
        selectFolder("Select a folder to save VCF files:", "folderSelected");
	}

}
void folderSelected(File selection){
    if(selection == null){
        println("folder not selected");
        isWritingFile = false;
    }else{
        String path = selection.getAbsolutePath()+"/";
        println("absolute path="+path);
        write(path);
    }
}


void write(String path){
    println("Start writing");
    if(isTestMode){
        for(int i = 0; i<_test_files.length; i++){
            //update view
            // draw();
            String file = _test_files[i];
            loadingSampleIndex = i;
            readAndWrite(file, path);
        }
        println("End of writing");
        JOptionPane.showMessageDialog(null, "Files are saved.","VCF export",JOptionPane.PLAIN_MESSAGE);

        isWritingFile = false;
    }else{
        for(int i = 0; i<_files.length; i++){
            //update view
            // draw();
            File file = _files[i];
            loadingSampleIndex = i;
            readAndWrite(file, path);
        }
        println("End of writing");
        JOptionPane.showMessageDialog(null, "Files are saved.","VCF export",JOptionPane.PLAIN_MESSAGE);
        isWritingFile = false;
            
    }
}
