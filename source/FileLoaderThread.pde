// File[] _files;
String[] _test_files;
File[] _files = new File[3];
int loadingSampleIndex;
long fileSize = 0;
long currentLoaded = 0;

int _FORMAT_AD_INDEX = -1; //index for AD element in VCF, since all three files may be structurally different.


boolean isTestMode = false;  //**************************************

class FileLoaderThread implements Runnable{
	void run() {
        //file chooser
        //assueme three _test_files are selected
        if(isTestMode){
            _test_files = new String[3];
            _test_files[0] = "Michelin4_test.vcf";
            _test_files[1] = "Michelin5_test.vcf";
            _test_files[2] = "Michelin3_test.vcf";

            // _test_files[0] = "Michelin4.vcf";
            // _test_files[1] = "Michelin5.vcf";
            // _test_files[2] = "Michelin3.vcf";
            
            for(int i = 0; i<_test_files.length; i++){
                String file = _test_files[i];
                String[] fileName = splitTokens(file, ".");
                loadingSampleIndex = i;
                _sampleLabels[i] = fileName[0];
                loadDataVCF(file);
            }
            _PROGRESS_MESSSAGE = "All files are loaded, and now processing";
            println("--- done loading file"); 
            initialize();
        }else{
            for(int i = 0; i<_files.length; i++){
                File file = _files[i];
                String[] fileName = splitTokens(file.getName(), ".");
                loadingSampleIndex = i;
                _sampleLabels[i] = fileName[0];
                loadDataVCF(file);
            }
            _PROGRESS_MESSSAGE = "All files are loaded, and now processing";
            println("--- done loading file"); 
            initialize();
        }
    }
}
//test mode
void loadDataVCF(String file) {
    //reset format ad index
    _FORMAT_AD_INDEX = -1;
    _PROGRESS_MESSSAGE = "Loading "+file+" ...";
    File f = new File(dataPath(file));
    fileSize = f.length();
    currentLoaded = 0; 
    // println(file+" file size ="+fileSize);
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(file);
        //Loop to read the file one line at a time
        String line = null;
        while ((line = reader.readLine()) != null) {
            currentLoaded += line.length();
            parseInfoVCF(line);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
}
//real mode
void loadDataVCF(File file) {
    //reset format ad index
    _FORMAT_AD_INDEX = -1;
    _PROGRESS_MESSSAGE = "Loading "+file.getAbsolutePath()+" ...";
    File f = file;
    fileSize = f.length();
    currentLoaded = 0; 
    // println(file+" file size ="+fileSize);
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(file);
        //Loop to read the file one line at a time
        String line = null;
        while ((line = reader.readLine()) != null) {
            currentLoaded += line.length();
            parseInfoVCF(line);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
}

void parseInfoVCF(String line) {
    if(line.startsWith("##")){
        //attribute
    }else if(line.startsWith("#")){
        //header
    }else{
        String[] s = split(line, TAB);
        if (s[0] != null) {
	        String chr = s[0].trim();
	        int chrIndex = data.getChrIndex(chr);
	        if(chrIndex == -1){
                println("Error:FileLoaderThread.parseInfoVCF():chr="+chr);
	            return;
	        }
	        int position = Integer.parseInt(s[1]);
	        String genotypeID = s[2];
	        String[] formats = splitTokens(s[9], ":");
	        if(_FORMAT_AD_INDEX == -1){
	            //find the AD index
	            String formatOrder = s[8];
	            String[] formatLabel = splitTokens(formatOrder, ":");
	            for(int i = 0; i< formatLabel.length; i++){
	                if(formatLabel[i].equals("AD")){
	                    this._FORMAT_AD_INDEX = i;
	                    break;
	                }
	            }   
        	}
        
	        //split format information
	        String[] ad = split(formats[_FORMAT_AD_INDEX], ',');
	        int refCount = Integer.parseInt(ad[0]);
	        int altCount = Integer.parseInt(ad[1]);
	        int totalCount = refCount + altCount;
	        int varfreq = round(((float)altCount / (float)totalCount)*100f);
	        
	        //find sample
	        Sample sample = data.findSample(loadingSampleIndex);
	        Variant variant = data.findVariant(chrIndex, position, genotypeID);
	        Row row = new Row(variant, totalCount, varfreq, loadingSampleIndex);
	        
	        sample.addRow(row);
	        variant.addRow(row);

        } else {
            System.out.println("string content is null");
        }
    }
}

