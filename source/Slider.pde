class Slider{
	int sampleIndex;
	int handleSize = 2;
	int value_min, value_max, value_current;
	Rectangle d_rect;
	int display_current_x;
	Rectangle handle_rect;


	//constructor
	Slider(int n, int min, int max, int def, int dx, int dy, int dw, int dh){
		sampleIndex = n;
		value_min = min;
		value_max = max;
		value_current = def;
		d_rect = new Rectangle(dx, dy, dw, dh);
	}

	void update(){
		display_current_x = round(map(value_current, value_min, value_max, d_rect.x, d_rect.x+d_rect.width));
		handle_rect = new Rectangle(display_current_x-handleSize, d_rect.y, handleSize*2,d_rect.height);
		// println("display rect="+d_rect);
		// println("handle rect="+handle_rect);

	}

	//update the value based on the mouse position
	void updateValue(int mx){
		display_current_x = constrain(mx, d_rect.x, d_rect.x+d_rect.width);
		handle_rect = new Rectangle(display_current_x-handleSize, d_rect.y, handleSize*2,d_rect.height);
		value_current = round(map(display_current_x, d_rect.x, d_rect.x+d_rect.width, value_min, value_max));
	}


}
