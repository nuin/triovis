class OptimisationThread implements Runnable{
	// float[][][] f_scores;
	int from;
	int to;
	int length;

	float highest_score = 0; 
	int[] highest_index = new int[3];

	boolean doneOnce = false;

	int totalIteration;
	int currentIndex;

	OptimisationThread(int from, int to){
		this.from = from;
		this.to = to;
		this.length = to - from +1;
		// this.f_scores = new float[length][length][length];

		currentIndex = 0;
		totalIteration = length*length*length;

	}

	void run(){
		if(!doneOnce){
			//calculate f score
			int tp_fn = globalInitialCount[0];

			for(int i = from; i < length; i++){
				coverageSliders[0].value_current = i;
				coverageSliders[0].update();
				data.updateCoverageNonThread(data.samples[0], i);

				for(int j = from; j < length; j++){
					coverageSliders[1].value_current = i;
					coverageSliders[1].update();
					data.updateCoverageNonThread(data.samples[1], j);
					for(int k = from; k < length; k++){
						//new coverage i, j, k
						coverageSliders[2].value_current = i;
						coverageSliders[2].update();
						data.updateCoverageNonThread(data.samples[2], k);

						int[] gcount = data.getGlobalCounts();
						int tp = gcount[0];
						int tp_fp = tp+ gcount[1];

						float precision = (float)tp / (float)tp_fp;
						float recall = (float)tp /(float)tp_fn;
						float fscore = 2*((precision*recall)/(precision + recall));

						if(fscore > highest_score){
							highest_score = fscore;
							highest_index[0] = i;
							highest_index[1] = j;
							highest_index[2] = k;
						}

						currentIndex ++;
						loop();
						// println("***** deubug: gcount = "+Arrays.toString(gcount) +" i="+i+"  j="+j+"  k="+k +"  score ="+fscore);
					}
				}
			}
			println("optimisation finished: highest score ="+highest_score+"  index:"+Arrays.toString(highest_index));
			doneOnce = true;
			isOptimising = false;
			// predictor.updateAll();
			// hardReset();
            setOptimal();
		}else{
			
		}
	}
	//set optimal score
	void setOptimal(){
		println("set optimal");
		for(int i = 0; i < 3; i++){
			coverageSliders[i].value_current = highest_index[i];
			coverageSliders[i].update();
			data.updateCoverageNonThread(data.samples[i], highest_index[i]);
		}
		predictor.updateAll();

	}
}
