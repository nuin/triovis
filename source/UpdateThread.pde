class UpdateThread extends Thread{
	boolean running;
	int wait;
	String id;
	int count;
	boolean increasing;

	ArrayList<Variant> variants;

	ConcurrentHashMap<String, Variant> variantMap;


	// UpdateThread(ArrayList<Variant> vs){
	// 	running = false;
	// 	variants = vs;
	// }
	UpdateThread(ConcurrentHashMap<String, Variant> vs){
		running = false;
		variantMap = vs;
	}

	UpdateThread(ConcurrentHashMap<String, Variant> vs, boolean b){
		running = false;
		variantMap = vs;
		increasing = b;
	}

	void start(){
		isUpdating = true;
		running = true;
		super.start();
	}

	void run(){
		Iterator ite = variantMap.entrySet().iterator();

		while(running && ite.hasNext()){
			Map.Entry entry = (Map.Entry)ite.next();
			//action
			// count++;
			Variant v = (Variant) entry.getValue();
            if(_MIGRATING){
                int[] newIndex = data.getVariantIndex(v);
                int[] oldIndex = v.cellIndex;
                if(oldIndex == null){
                    //variants outside of the varfreq range
                }else{
                    // println("old index:"+Arrays.toString(v.cellIndex)+"  new index:"+Arrays.toString(newIndex)+ v.rows.get(0).coverage);
                    // if(increasing){

                    // }
                    //remove variant from the old cell
                    Cell cell = data.table[oldIndex[0]][oldIndex[1]][oldIndex[2]];
                    cell.removeVariant(v);
                    // println(Arrays.toString(oldIndex)+": total = "+cell.variants.size());
                    //add variant to the new cell
                    cell = data.table[newIndex[0]][newIndex[1]][newIndex[2]];
                    cell.addVariant(v);
                }
            }else{
                int[] cellIndex  = v.cellIndex;
                if(cellIndex == null){
                    //variants outside of the Varfreq Reange
                }else{
                    Cell cell = data.table[cellIndex[0]][cellIndex[1]][cellIndex[2]];
                    if(increasing){
                    	cell.removeVariant(v);
                    }else{
                    	cell.addVariant(v);
                    }
                }
            }     
		}
		isUpdating = false;
		
		// predictor.updateAll();//update preditor
		// println("UpdateThread is done!");

	}

	void quit(){
		println("UpdateThread quitting");
		running = false;
		interrupt();
	}

	void addVariant(ArrayList<Variant> vs){
		// for(int i = 0; i< vs.size(); i++){
		// 	if(vari)
		// }
		variants.addAll(vs);
	}


}
