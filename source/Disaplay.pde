
int MARGIN = 10;
int Y_MARGIN = 50;
int LABEL_MARGIN = MARGIN / 5;
// int STAGE_WIDTH = 1200;
// int STAGE_HEIGHT = 800;
int plotX1 = MARGIN * 9;
int plotY1 = MARGIN * 6;
int cellWidth = MARGIN * 32;
int cellHeight = MARGIN * 5;
int blockHeight = cellHeight * 3;
int cellMaxCount = 400;
int cellMaxCoverage = 200;
int cellMinCoverage = 0;
int plotWidth = (cellWidth * 3) + (MARGIN * 2);
int plotHeight = (blockHeight * 3) + (MARGIN * 2);
int plotX2 = plotX1 + plotWidth;
int plotY2 = plotY1 + plotHeight;
//global plot
int globalBarW = 20;
int globalPlotW = (globalBarW * 2) + (MARGIN * 3);
int globalPlotX1 = plotX2 + (MARGIN * 2);
int globalPlotX2 = globalPlotX1 + globalPlotW;
int globalPlotY1 = plotY1;
int globalPlotY2 = plotY2;
int globalPlotH = plotHeight;
int globalPossibleX = globalPlotX1 + MARGIN;
int globalImpossibleX = this.globalPossibleX + globalBarW + MARGIN;
int tableX[] = {plotX1, plotX1 + MARGIN + cellWidth, plotX1 + (MARGIN * 2) + (cellWidth * 2)};
int tableY[] = {plotY1, plotY1 + (MARGIN) + blockHeight, plotY1 + (MARGIN * 2) + (blockHeight * 2)};

//controller coordinates
int varfreqX = plotX1;
int varfreqY = plotY2 + (MARGIN * 5);// +150;
int varfreqWidth = cellWidth;//MARGIN * 20;
int varfreqHeight = MARGIN * 4;
int varfreqHandleSize = 5;
//coverage slider
int coverageX = varfreqX + varfreqWidth + (MARGIN);
int coverageY = varfreqY;
int coverageWidth = cellWidth - MARGIN; //subtract the button width
int coverageHeight = varfreqHeight;
int coverageSliderMin = 1;
int coverageSliderMax = 200; 
int controllerGapY = MARGIN; //gap

int coverageMaxCount = -1; 

//predictor
int predictorX  = coverageX + coverageWidth + (MARGIN)*2;
int predictorY = coverageY;
int predictorWidth = cellWidth;
int predictorHeight = varfreqHeight*3+MARGIN*2;//this.blockHeight;
Rectangle predictorRect = new Rectangle(predictorX, predictorY, predictorWidth, predictorHeight);

//global count
int[] globalInitialCount;
int gInitialHeightPossible = 0;
int gInitialHeightImpossible = 0;
int globalBarInterval = 1000;
int globalMaxCount = 0;


int updateMsgX = predictorX;
int updateMsgY = predictorY+predictorHeight+MARGIN*2;


//File Choose page
int instructionX = plotX1;
int instructionY = plotY1;
int p1_y = plotY1 + MARGIN*4;
int p2_y = p1_y + MARGIN*3;
int c_y = p2_y + MARGIN*3;
int label_x_r = plotX1 + MARGIN*7;
int path_x = label_x_r +MARGIN;
int path_w = MARGIN*40;
int path_h = MARGIN*2;
Rectangle p1_path_rect = new Rectangle(path_x, p1_y, path_w, path_h);
Rectangle p2_path_rect = new Rectangle(path_x, p2_y, path_w, path_h);
Rectangle c_path_rect = new Rectangle(path_x, c_y, path_w, path_h);
Rectangle[] path_rects = {p1_path_rect, p2_path_rect, c_path_rect};
int btn_x = path_x+path_w+MARGIN;
int btn_w = MARGIN*5;
int btn_h = MARGIN*2;
Rectangle p1_btn = new Rectangle(btn_x, p1_y, btn_w, btn_h);
Rectangle p2_btn = new Rectangle(btn_x, p2_y, btn_w, btn_h);
Rectangle c_btn = new Rectangle(btn_x, c_y, btn_w, btn_h);

int btn_go_y = c_y+MARGIN*3;
Rectangle go_btn = new Rectangle(btn_x, btn_go_y, btn_w, btn_h);

int annotateit_x = plotX1;
int annotateit_msg_y = c_btn.y+c_btn.height+MARGIN*10;
int annotateit_path_y = annotateit_msg_y+MARGIN*3;
Rectangle a_path_rect = new Rectangle(path_x, annotateit_path_y, path_w, path_h);
Rectangle annotateit_btn = new Rectangle(btn_x, annotateit_path_y, btn_w, btn_h);

int btn_go2_y = annotateit_path_y+MARGIN*3;
Rectangle go2_btn = new Rectangle(btn_x, btn_go2_y, btn_w, btn_h);
Rectangle[] histogram_rects = null;
int selected_bar = -1;





String[] tableLabels = {"Ref/Ref", "Ref/Alt", "Alt/Alt"};



//set the cell display coordinates, assign Rectangle per Cell
void setupCellCoordinates(){
	Cell[][][] table = data.table;
    Rectangle rect = table[0][0][0].rect;
    if (rect == null) {
        //first time set rectangle positions
        for (int i = 0; i < table.length; i++) {
            int runningX = tableX[i];
            for (int j = 0; j < table.length; j++) {
                int runningY = tableY[j];
                for (int k = 0; k < table.length; k++) {
                    Cell cell = table[i][j][k];
                    cell.rect = new Rectangle(runningX, runningY, this.cellWidth, this.cellHeight);
                    runningY += cellHeight;
                }
            }
        }
    }
}
//draw varfreq distribution slider   
void drawVarfreqDistibution() {
    for (int i = 0; i < _sampleLabels.length; i++) {
        Sample s = data.samples[_sampleOrder[i]];
        String sampleID = s.sampleID;
        Distribution distribution = s.varfreqDistribution;

        //range slider position
        Range range = varfreqRanges[i];
        Rectangle r = range.display_rect;
        int[] counts = distribution.countArray;
        int maxCount = distribution.maxCount;
        int minCount = distribution.minCount;

        //draw background
        fill(255);
        noStroke();
        rectMode(CORNERS);
        rect(r.x, r.y, r.x+r.width, r.y+r.height);
        //draw distribution
        fill(this.colorSampleArray[i]);  // color based on sample
        noStroke();
        beginShape();
        vertex(r.x, r.y + r.height);
        for (int j = 0; j < counts.length; j++) {
            int count = counts[j];
            float py = map(count, minCount, maxCount, r.y + r.height, r.y);
            float px = map(j, 0, counts.length - 1, r.x, r.x + r.width);
            vertex(px, py);
        }
        vertex(r.x + r.width, r.y + r.height);
        endShape(CLOSE);
        
        int r_low = range.value_current_low;
        int r_high = range.value_current_high;
        float px1 = range.dx_low;//map(r_low, _VARFREQ_MIN, _VARFREQ_MAX, r.x,  r.x+r.width);
        float px2 = range.dx_high;//map(r_high, _VARFREQ_MIN, _VARFREQ_MAX, r.x,  r.x+r.width);
        //current value
        //line
        stroke(colorText);
        strokeWeight(1f);
        line(px1, r.y, px1, r.y+r.height);
        line(px2, r.y, px2, r.y+r.height);
        //circle
        stroke(colorText);
        strokeWeight(3f);
        point(px1, r.y+r.height);
        point(px2, r.y+r.height);
        //text
        fill(colorText);
        textFont(_FONT);
        textSize(12f);
        textAlign(LEFT, TOP);
        text(r_low, px1+1, r.y);
        text(r_high, px2+1, r.y);

        //mouseover highlight
        if(range.h_low.contains(mouseX, mouseY)){
            //highlight the selected region
            Rectangle h = range.h_low;
            rectMode(CORNER);
            fill(color_cyan_trans);
            noStroke();
            rect(h.x, h.y, h.width, h.height);
        }else if(range.h_high.contains(mouseX, mouseY)){
            //highlight the selected region
            Rectangle h = range.h_high;
            rectMode(CORNER);
            fill(color_cyan_trans);
            noStroke();
            rect(h.x, h.y, h.width, h.height);
        }
    }
}
//draw coverage distribution slider   
void drawCoverageDistribution() {
    if(coverageMaxCount == -1){
        //calculate the average of max count for distribution
        int sum = 0;
        for (int i = 0; i < _sampleLabels.length; i++) {
            Sample s = data.samples[_sampleOrder[i]];
            Distribution distribution = s.coverageDistribution;
            int maxCount = distribution.maxCount;
            sum += maxCount;
        }
        coverageMaxCount = sum/_sampleLabels.length;
        println("average is ="+coverageMaxCount);
    }

    for (int i = 0; i < _sampleLabels.length; i++) {
        Sample s = data.samples[_sampleOrder[i]];
        Distribution distribution = s.coverageDistribution;
        Slider slider = coverageSliders[i];
        Rectangle r = slider.d_rect;

        int[] counts = distribution.countArray;
        // int maxCount = distribution.maxCount;
        int maxCount = coverageMaxCount;
        int minCount = distribution.minCount;
        int value = slider.value_current;
        int dx = slider.display_current_x;//map(value, _COVERAGE_MIN, _COVERAGE_MAX, r.x, r.x+r.width);
        
        // println("maxCount ="+distribution.maxCount +" sample="+s.sampleID);
        //background
        fill(255);
        noStroke();
        rectMode(CORNER);
        rect(r.x, r.y, r.width, r.height);

        //draw distribution
        fill(colorSampleArray[i]);
        noStroke();
        beginShape();
        vertex(r.x, r.y + r.height);
        for (int j = 0; j < counts.length; j++) {
            int count = counts[j];
            float py = constrain(map(count, minCount, maxCount, r.y + r.height, r.y),r.y , r.y+r.height);
            float px = map(j, 0, counts.length - 1, r.x, r.x + r.width);
            vertex(px, py);
        }
        vertex(r.x + r.width, r.y + r.height);
        endShape(CLOSE);
        
        
        // current value
        noFill();
        stroke(colorText);
        strokeWeight(1f);
        line(dx, r.y, dx, r.y+r.height);
        //dot
        strokeWeight(3f);
        point(dx, r.y+r.height);
        //text
        fill(colorText);
        textSize(12f);
        textAlign(LEFT, TOP);
        text(value, dx+1, r.y);

        //mouse over
        if(slider.handle_rect.contains(mouseX, mouseY)){
            //highlight the selected region
            Rectangle h = slider.handle_rect;
            rectMode(CORNER);
            fill(color_cyan_trans);
            noStroke();
            rect(h.x, h.y, h.width, h.height);
        }
    }
}
//draw up and down btns for coverage
void drawUpAndDownBtns() {
    for (int i = 0; i < upBtns.length; i++) {
        Rectangle up = upBtns[i];
        Rectangle down = downBtns[i];
        rectMode(CORNER);
        noStroke();

        //up
        if (up.contains(mouseX, mouseY)) {
            fill(colorDarkGray);
            _currentBtnSampleIndex = i;
        } else {
            fill(colorLightGray);
        }
        rect(up.x, up.y, up.width, up.height);
        if (up.contains(mouseX, mouseY)) {
            fill(colorLightGray);
        } else {
            fill(colorDarkGray);
        }
        triangle(up.x + up.width / 2, up.y + up.height / 3, up.x + up.width - 2, up.y + 2 * up.height / 3, up.x + 2, up.y + 2 * up.height / 3);


        //down
        if (down.contains(mouseX, mouseY)) {
            fill(colorDarkGray);
            _currentBtnSampleIndex = i;
        } else {
            fill(colorLightGray);
        }
        rect(down.x, down.y, down.width, down.height);
        if (down.contains(mouseX, mouseY)) {
            fill(colorLightGray);
        } else {
            fill(colorDarkGray);
        }
        triangle(down.x + down.width / 2, down.y + 2 * down.height / 3, down.x + up.width - 2, down.y + up.height / 3, down.x + 2, down.y + down.height / 3);
    }
}
//draw controller
void drawControllerLabels() {
    //sample id
    textFont(_FONT);
    textSize(14);
    for (int i = 0; i < _sampleOrder.length; i++) {
        String sampleId = _sampleLabels[_sampleOrder[i]];
        textAlign(RIGHT, TOP);
        fill(colorSampleArray[i]);
        text(sampleId, plotX1 - MARGIN, varfreqY + (i * (MARGIN + varfreqHeight)));
    }

    fill(colorText);
    //sample ID
    textAlign(RIGHT, BOTTOM);
    text("SampleID", plotX1 - MARGIN, varfreqY);

    //controller label
    for (int i = 0; i < controllerLabel.length; i++) {
        textAlign(LEFT, BOTTOM);
        text(controllerLabel[i], controllerLabelX[i], varfreqY);
    }
}
//draws prector
void drawPredictor_archive() {
    //background
    fill(255);
    noStroke();
    rect(predictorX, predictorY, predictorWidth, predictorHeight);
    //draw grid
    noFill();
    stroke(colorBackground);
    strokeWeight(1f);
    //vertical
    for(int i = 1; i<predictor.binSize-1;i++){
        float px = map(i, 0, predictor.binSize-1, predictorX, predictorX+predictorWidth);
        line(px, predictorY, px, predictorY+predictorHeight);
    }
    //horizontal
    for(int i = predictor.countInterval; i<predictor.countMax; i+=predictor.countInterval){
        float py = map(i, predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY);
        line(predictorX, py, predictorX+predictorWidth, py);
    }

    //data
    int[][][] counters = predictor.counters;
    //sample
    for(int i = 0; i < 3; i++ ){
        int[][] sample = counters[i];
        //possible
        int[] possible = sample[0];
        //impossible
        int[] impossible = sample[1];
        
        //possible
        if(_currentBtnSampleIndex == i){
            stroke(colorSampleArray[i]);
        }else{
            stroke(colorGrayTrans);
        }
        float px = 0;
        float py = 0;
        strokeWeight(1f);
        noFill();
        beginShape();
        for(int j = 0; j< possible.length; j++){
            px = map(j, 0, possible.length-1, predictorX, predictorX+predictorWidth);
            py = constrain(map(possible[j], predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY), predictorY, predictorY+predictorHeight);
            vertex(px, py);
        }
        endShape();
        if(_currentBtnSampleIndex == i){
            fill(colorSampleArray[i]);
            textAlign(LEFT, BOTTOM);
            textSize(12);
            text("possible", px+1, py);
            stroke(colorSampleArray[i]);
        }else{
            stroke(colorGrayTrans);
        }
        
        //impossible
        strokeWeight(2f);
        noFill();
        beginShape();
        for(int j = 0; j< impossible.length; j++){
            px = map(j, 0, impossible.length-1, predictorX, predictorX+predictorWidth);
            py = constrain(map(impossible[j], predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY), predictorY, predictorY+predictorHeight);
            vertex(px, py);
        }
        endShape();
        if(_currentBtnSampleIndex == i){
            fill(colorSampleArray[i]);
            textAlign(LEFT, BOTTOM);
            textSize(12);
            text("impossible", px+1, py);
            stroke(colorSampleArray[i]);
        }else{
            stroke(colorGrayTrans);
        }
        
        //draw current coverage
        int coverage = data.samples[_sampleOrder[i]].currentCoverage;//sampleMinCoverages[i];
        px = constrain(map(coverage, 1, impossible.length, predictorX, predictorX+predictorWidth), predictorX, predictorX+predictorWidth);
        stroke(colorSampleArray[i]);
        strokeWeight(1f);
        line(px, predictorY, px, predictorY+predictorHeight+ (i*5));
        fill(colorSampleArray[i]);
        ellipse(px, predictorY+predictorHeight+ (i*5), 5, 5);
        
    }
        
        
    //coverage label
    fill(colorText);
    textAlign(CENTER, TOP);
    textSize(10);
    for(int i = 0; i<predictor.binSize;i++){
        int index = i+1;
        if(index == 1 || index%10 == 0){
            float px = map(i, 0, predictor.binSize-1, predictorX, predictorX+predictorWidth);
            text((index),px, predictorY+predictorHeight);
        }
    }
}
//two bar graphs
void drawPredictor_two_bars() {
    noFill();
    stroke(255);
    strokeWeight(1f);
    //vertical
    for(int i = 1; i<predictor.binSize-1;i++){
        float px = map(i, 0, predictor.binSize-1, predictorX, predictorX+predictorWidth);
        line(px, predictorY, px, predictorY+predictorHeight);
    }
    //horizontal
    for(int i = predictor.countInterval; i<predictor.countMax; i+=predictor.countInterval){
        float py = map(i, predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY);
        line(predictorX, py, predictorX+predictorWidth, py);
    }

    float increment = ((float)predictorWidth/(float)(predictor.binSize))/2;

    //data
    int[][][] counters = predictor.counters;
    if(_currentBtnSampleIndex != -1){
        int[][] sample = counters[_currentBtnSampleIndex];
        //possible
        int[] possible = sample[0];
        //impossible
        int[] impossible = sample[1];
        rectMode(CORNERS);
        float px = 0;
        float py = 0;
        for(int j = 0; j< possible.length; j++){
            px = map(j, 0, possible.length-1, predictorX, predictorX+predictorWidth);
            py = constrain(map(possible[j], predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY), predictorY, predictorY+predictorHeight);
            //possible
            stroke(colorText);
            fill(colorPossible);
            rect(px, predictorY+predictorHeight, px+increment, py);

            //impossible
            px += increment;
            py = constrain(map(impossible[j], predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY), predictorY, predictorY+predictorHeight);
            fill(colorImpossible);
            rect(px, predictorY+predictorHeight, px+increment, py);
        }
        //draw current coverage
        int coverage = data.samples[_sampleOrder[_currentBtnSampleIndex]].currentCoverage;//sampleMinCoverages[i];
        px = constrain(map(coverage, 1, predictor.binSize, predictorX, predictorX+predictorWidth), predictorX, predictorX+predictorWidth);
        stroke(colorSampleArray[_currentBtnSampleIndex]);
        strokeWeight(1f);
        line(px, predictorY, px, predictorY+predictorHeight+ (_currentBtnSampleIndex*5));
        fill(colorSampleArray[_currentBtnSampleIndex]);
        ellipse(px, predictorY+predictorHeight+ (_currentBtnSampleIndex*5), 5, 5);
    }
        
    //coverage label
    fill(colorText);
    textAlign(CENTER, TOP);
    textSize(10);
    for(int i = 0; i<predictor.binSize;i++){
        int index = i+1;
        if(index == 1 || index%10 == 0){
            float px = map(i, 0, predictor.binSize-1, predictorX, predictorX+predictorWidth);
            text((index),px+increment, predictorY+predictorHeight);
        }
    }
}
//stacked
void drawPredictor() {
    noFill();
    stroke(255);
    strokeWeight(1f);  

    float increment = ((float)predictorWidth/(float)(predictor.binSize));
    //vertical grid
    float runningX = predictorX;
    for(int i = 0; i<=predictor.binSize;i++){
        line(runningX, predictorY, runningX, predictorY+predictorHeight);
        runningX += increment;
    }
    //horizontal grid
    for(int i = 0; i<=predictor.countMax; i+=predictor.countInterval){
        float py = map(i, predictor.countMin, predictor.countMax, predictorY+predictorHeight, predictorY);
        line(predictorX, py, predictorX+predictorWidth, py);
    }


    //data
    int[][][] counters = predictor.counters;
    if(_currentBtnSampleIndex != -1){
        //draw color overlay
        fill(colorSampleArray[_currentBtnSampleIndex], 60);
        noStroke();
        rectMode(CORNER);
        rect(predictorRect.x, predictorRect.y, predictorRect.width, predictorRect.height);

        histogram_rects = new Rectangle[predictor.binSize];
        int[][] sample = counters[_currentBtnSampleIndex];
        //possible
        int[] possible = sample[0];
        //impossible
        int[] impossible = sample[1];
        rectMode(CORNER);
        float px = predictorX;
        for(int j = 0; j< possible.length; j++){
            float ph1 = constrain(map(possible[j], predictor.countMin, predictor.countMax, 0, predictorHeight), 0, predictorHeight);
            float py = predictorY+predictorHeight - ph1; //LEFT TOP corner
            //possible
            stroke(colorText);
            fill(colorPossible);
            rect(px, py, increment, ph1);

            //impossible
            float ph2 = constrain(map(impossible[j], predictor.countMin, predictor.countMax, 0, predictorHeight), 0, predictorHeight);
            py -= ph2; //LEFT TOP CORNER
            fill(colorImpossible);
            rect(px, py, increment, ph2);

            //record rectange
            histogram_rects[j] = new Rectangle(round(px), round(py), round(increment), round(ph1+ph2)); ////////////////////

            //if selected
            if(j == selected_bar){
                stroke(colorCyan);
                fill(color_cyan_trans);
                rect(round(px), round(py), round(increment),round(ph1+ph2));

                drawVariantCounts( predictor.variant_counters[_currentBtnSampleIndex][j]);
                // println("selected:");
                // println(Arrays.deepToString(predictor.variant_counters[_currentBtnSampleIndex][j]));

            }


            //increment
            px += increment;
        }
        //draw current coverage
        int coverage = data.samples[_sampleOrder[_currentBtnSampleIndex]].currentCoverage;//sampleMinCoverages[i];
        px = constrain(map(coverage, 1, predictor.binSize+1, predictorX, predictorX+predictorWidth), predictorX, predictorX+predictorWidth);
        stroke(colorSampleArray[_currentBtnSampleIndex]);
        strokeWeight(1f);
        line(px, predictorY, px, predictorY+predictorHeight+ MARGIN);//(_currentBtnSampleIndex*5));
        fill(colorSampleArray[_currentBtnSampleIndex]);
        ellipse(px, predictorY+predictorHeight+MARGIN , 5,5); // (_currentBtnSampleIndex*5), 5, 5);
    }else{
        // println("debug: current sample index is "+_currentBtnSampleIndex);
        histogram_rects = null;
    }

        
    //coverage label
    fill(colorText);
    textAlign(CENTER, TOP);
    textSize(10);
    for(int i = 1; i<=predictor.binSize+1;i++){
        if(i == 1 || i%10 == 0){
            float px = map(i, 1, predictor.binSize+1, predictorX, predictorX+predictorWidth);
            text((i),px+(increment/2), predictorY+predictorHeight);
        }
    }
}
void drawTable() {
    rectMode(CORNERS);
    Cell[][][] table = data.table;

    Cell mouseOverCell = null;
    //drawing cells
    for (int i = 0; i < table.length; i++) {
        for (int j = 0; j < table.length; j++) {
            for (int k = 0; k < table.length; k++) {
                Cell cell = table[i][j][k];
                Rectangle rec = cell.rect;
                int[][] countArrays = cell.getCountArrays();                

                if (i == 0 && j == 0 && k == 0) {
                    fill(colorBackground);
                    stroke(colorImpossible);
                    strokeWeight(1f);
                    rect(rec.x, rec.y, rec.x + cellWidth, rec.y + cellHeight);
                } else {
                    //inner 
                    if (_combination[i][j][k]) {
                        fill(colorPossible);
                    } else {
                        fill(colorImpossible);
                    }
                    noStroke();
                    rect(rec.x, rec.y, rec.x + cellWidth, rec.y + cellHeight);
                }

                //draw distribution;
                for (int n = 0; n < countArrays.length; n++) {
                    int[] countArray = countArrays[n];//cd.getCountArray(n);
                    int maxX = cell.maxCoverage;
                    int minX = cell.minCoverage;
                    int minY = 1;
                    fill(colorTransSampleArray[n]);
                    noStroke();
                    drawDistributionGraph(countArray, rec.x, rec.y, cellWidth, cellHeight, minX, maxX, minY, cellMaxCount);
                }

                //draw distribution outline
                for (int n = 0; n < countArrays.length; n++) {
                    int[] countArray = countArrays[n];//cd.getCountArray(n);
                    int maxX = cell.maxCoverage;
                    int minX = cell.minCoverage;
                    int minY = 1;
                    noFill();
                    stroke(colorSampleArray[n]);
                    strokeWeight(1f);
                    drawDistributionGraph(countArray, rec.x, rec.y, cellWidth, cellHeight, minX, maxX, minY, cellMaxCount);
                }
                //// MOUSE OVER ///////////////////////////////////////////////////////
                // if the mouse hover over the cell or selected
                if (cell.rect.contains(mouseX, mouseY) || cell.isSelected) {
                    noStroke();
                    fill(colorSelectionTrans);
                    rect(rec.x, rec.y, rec.x + cellWidth, rec.y + cellHeight);

                    //cell label
                    noStroke();
                    fill(colorWhite);
                    textSize(12);
                    textAlign(LEFT, TOP);
                    text(_cellLable[i][j][k], rec.x + LABEL_MARGIN, rec.y + LABEL_MARGIN);

                    //cell info
                    int variantCount = cell.variants.size();
                    textAlign(RIGHT, TOP);
                    text("variant:" + variantCount, rec.x + cellWidth - LABEL_MARGIN, rec.y + LABEL_MARGIN);

                    if(cell.rect.contains(mouseX, mouseY)){
                        mouseOverCell = cell;
                    }
                }
            }
        }
    }
    if(mouseOverCell != null){
        drawCellMouseOver(mouseOverCell);
    }
}

//draw scale and grid
void drawCellMouseOver(Cell cell){    
    Rectangle rec = cell.rect;
    //show scale
    stroke(80);
    strokeWeight(1);
    noFill();
    line(rec.x, rec.y, rec.x, rec.y+rec.height);
    line(rec.x, rec.y+rec.height, rec.x+rec.width , rec.y+rec.height);
    //y ticks
    line(rec.x, rec.y, rec.x+10, rec.y);
    //middle
    line(rec.x, rec.y+(rec.height/2), rec.x+5,rec.y+(rec.height/2));
    //x ticks
    line(rec.x+rec.width, rec.y+rec.height, rec.x+rec.width, rec.y+rec.height-10);
    //middle
    line(rec.x+(rec.width/2), rec.y+rec.height, rec.x+(rec.width/2), rec.y+rec.height-10);
    //50
    line(rec.x+(rec.width/4), rec.y+rec.height, rec.x+(rec.width/4), rec.y+rec.height-5);
    //150
    line(rec.x+(rec.width/4 *3), rec.y+rec.height, rec.x+(rec.width/4*3), rec.y+rec.height-5);






    // //draw scale label background
    // fill(255, 180);
    // noStroke();
    // rectMode(CORNERS);
    // rect(rec.x - 35, rec.y, rec.x, rec.y+12);
    // rect(rec.x -15, rec.y+rec.height - 12, rec.x, rec.y+rec.height);

    //text
    //y axis
    fill(80);
    textAlign(RIGHT, CENTER);
    text(cellMaxCount, rec.x, rec.y);
    textAlign(RIGHT, CENTER);
    text("0", rec.x, rec.y+rec.height);
    text("200",rec.x, rec.y+(rec.height/2));

    //x axis
    // fill(colorWhite);
    // textAlign(LEFT, BOTTOM);
    // text(_COVERAGE_MIN, rec.x, rec.y+rec.height);
    fill(80);
    textAlign(CENTER, TOP);
    text(_COVERAGE_MAX, rec.x+rec.width, rec.y+rec.height);
    text(100, rec.x+(rec.width/2), rec.y+rec.height);
    text(50, rec.x+(rec.width/4), rec.y+rec.height);
    text(150, rec.x+(rec.width/4 *3), rec.y+rec.height);
}

//function to draw distribution graph
void drawDistributionGraph(int[] countArray, int posX, int posY, int boxW, int boxH, int minX, int maxX, int minY, int maxY) {
    beginShape();
    vertex(posX, posY + boxH);
    for (int i = 0; i < countArray.length; i++) {
        int count = countArray[i];
        float py = constrain(map(count, minY, maxY, posY + boxH, posY), posY, posY + boxH);
        float px = map(i, 0, countArray.length - 1, posX, posX + boxW);
        vertex(px, py);

    }
    vertex(posX + boxW, posY + boxH);
    endShape();
}

void drawTableLabel() {
    //parent 1 - horizontal
    textAlign(LEFT, BOTTOM);
    textSize(30);
    float textY = plotY1 - (MARGIN * 2) + textDescent() - 2;

    for (int i = 0; i < tableLabels.length; i++) {
        fill(colorSampleArray[0]);
        text(tableLabels[i], tableX[i], textY);
    }

    //parent2 - vertical
    textAlign(LEFT, BOTTOM);
    textSize(20);
    int label2x = plotX1 - (MARGIN * 4);
    pushMatrix();
    translate(label2x, tableY[0] + blockHeight);
    rotate(-HALF_PI);
    fill(colorSampleArray[1]);
    text(tableLabels[0], 0, 0);
    translate(-(blockHeight + MARGIN), 0);
    text(tableLabels[1], 0, 0);
    translate(-(blockHeight + MARGIN), 0);
    text(tableLabels[2], 0, 0);
    popMatrix();

    //child
    textAlign(LEFT, BOTTOM);
    textSize(10);
    fill(colorSampleArray[2]);
    pushMatrix();
    translate(plotX1 - (MARGIN * 2), tableY[0] + cellHeight);
    rotate(-HALF_PI);
    text(tableLabels[0], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[1], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[2], 0, 0);

    translate(-MARGIN - cellHeight, 0);
    text(tableLabels[0], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[1], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[2], 0, 0);

    translate(-MARGIN - cellHeight, 0);
    text(tableLabels[0], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[1], 0, 0);
    translate(-(cellHeight), 0);
    text(tableLabels[2], 0, 0);
    popMatrix();


    //draw lines
    noFill();
    strokeWeight(1f);
    //parent1
    int y1 = plotY1 - (MARGIN * 2);
    int y2 = y1 + MARGIN;
    drawHorizontalAxisDeco(tableX[0], y1, tableX[0] + cellWidth, y2, colorSampleArray[0]);
    drawHorizontalAxisDeco(tableX[1], y1, tableX[1] + cellWidth, y2, colorSampleArray[0]);
    drawHorizontalAxisDeco(tableX[2], y1, tableX[2] + cellWidth, y2, colorSampleArray[0]);

    //parent2
    int x1 = plotX1 - (MARGIN * 4);
    int x2 = x1 + MARGIN;
    drawVerticalAxisDeco(x1, tableY[0], x2, tableY[0] + blockHeight, colorSampleArray[1]);
    drawVerticalAxisDeco(x1, tableY[1], x2, tableY[1] + blockHeight, colorSampleArray[1]);
    drawVerticalAxisDeco(x1, tableY[2], x2, tableY[2] + blockHeight, colorSampleArray[1]);

    //child
    int gap = 1;
    x1 += (MARGIN * 2);
    x2 += (MARGIN * 2);
    drawVerticalAxisDeco(x1, tableY[0] + gap, x2, tableY[0] + cellHeight - gap, colorSampleArray[2]);
    drawVerticalAxisDeco(x1, (tableY[0] + gap + cellHeight), x2, tableY[0] - gap + (cellHeight * 2), colorSampleArray[2]);
    drawVerticalAxisDeco(x1, tableY[0] + gap + cellHeight * 2, x2, tableY[0] - gap + (cellHeight * 3), colorSampleArray[2]);
    drawVerticalAxisDeco(x1, tableY[1] + gap, x2, tableY[1] + cellHeight - gap, colorSampleArray[2]);
    drawVerticalAxisDeco(x1, (tableY[1] + gap + cellHeight), x2, tableY[1] - gap + (cellHeight * 2), colorSampleArray[2]);
    drawVerticalAxisDeco(x1, tableY[1] + gap + cellHeight * 2, x2, tableY[1] - gap + (cellHeight * 3), colorSampleArray[2]);
    drawVerticalAxisDeco(x1, tableY[2] + gap, x2, tableY[2] + cellHeight - gap, colorSampleArray[2]);
    drawVerticalAxisDeco(x1, (tableY[2] + gap + cellHeight), x2, tableY[2] - gap + (cellHeight * 2), colorSampleArray[2]);
    drawVerticalAxisDeco(x1, tableY[2] + gap + cellHeight * 2, x2, tableY[2] - gap + (cellHeight * 3), colorSampleArray[2]);
}
void drawHorizontalAxisDeco(int x1, int y1, int x2, int y2, int c) {
    noFill();
    stroke(c);
    line(x1, y1, x1, y2);
    line(x1, y1, x2, y1);
    line(x2, y1, x2, y2);
}
void drawVerticalAxisDeco(int x1, int y1, int x2, int y2, int c) {
    noFill();
    stroke(c);
    line(x1, y1, x2, y1);
    line(x1, y1, x1, y2);
    line(x1, y2, x2, y2);
}
//draw grlobal count
void drawGlobalCount() {
    //record initial height
    if (gInitialHeightPossible == 0) {
        globalInitialCount = data.getGlobalCounts(); //get count
        //find global max count
        int higherCount = max(globalInitialCount[0], globalInitialCount[1]);
        //check the interval size, and adjust the interval size
        if(higherCount > 100000){
            globalBarInterval = 10000;
        }

        globalMaxCount = ((higherCount/globalBarInterval)+2)* globalBarInterval;

        int possibleHeight = round(map(globalInitialCount[0], 0, globalMaxCount, 0, globalPlotH));
        int impossibleHeight = round(map(globalInitialCount[1], 0, globalMaxCount, 0, globalPlotH));
        gInitialHeightPossible = globalPlotY2 - possibleHeight;
        gInitialHeightImpossible = globalPlotY2 - impossibleHeight;
        //debug
        println("max count = "+globalMaxCount);
        println("global initial variants count:" + Arrays.toString(globalInitialCount));
    }
    //draw grid
    stroke(colorWhite);
    strokeWeight(1f);
    noFill();
    int intervalCount = globalMaxCount / globalBarInterval;
    for (int i = 1; i < intervalCount; i++) {
        int count = globalBarInterval * i;
        int gridY = round(map(count, 0, globalMaxCount, globalPlotY2, globalPlotY2 - globalPlotH));
        line(globalPlotX1, gridY, globalPlotX2, gridY);
    }
    //axis
    stroke(colorWhite);
    strokeWeight(1f);
    line(globalPlotX1, globalPlotY2, globalPlotX2, globalPlotY2);
    line(globalPlotX2, globalPlotY1, globalPlotX2, globalPlotY2);

    //draw initial height
    rectMode(CORNERS);
    stroke(colorGray);
    strokeWeight(1f);
    noFill();
    rect(globalPossibleX, gInitialHeightPossible, globalPossibleX + globalBarW, globalPlotY2);
    rect(globalImpossibleX, gInitialHeightImpossible, globalImpossibleX + globalBarW, globalPlotY2);
    //intial count text
    fill(colorGray);
    textSize(12);
    textAlign(LEFT, BOTTOM);
    text(globalInitialCount[0], globalPossibleX, gInitialHeightPossible);
    text(globalInitialCount[1], globalImpossibleX, gInitialHeightImpossible);

    //draw bars
    int[] currentCount = data.getGlobalCounts();
    int possibleBar = round(map(currentCount[0], 0, globalMaxCount, 0, globalPlotH));
    int impossibleBar = round(map(currentCount[1], 0, globalMaxCount, 0, globalPlotH));

    stroke(colorText);
    fill(colorPossible);
    rect(globalPossibleX, globalPlotY2 - possibleBar, globalPossibleX + globalBarW, globalPlotY2);
    fill(colorImpossible);
    rect(globalImpossibleX, globalPlotY2 - impossibleBar, globalImpossibleX + globalBarW, globalPlotY2);
    //current count text
    fill(colorText);
    textAlign(LEFT, BOTTOM);
    textSize(12);
    text(currentCount[0], globalPossibleX, globalPlotY2 - possibleBar);
    text(currentCount[1], globalImpossibleX, globalPlotY2 - impossibleBar);

        //textlabel
    fill(colorText);
    noStroke();
    textSize(15);
    textLeading(15);
    textAlign(RIGHT, BOTTOM);
    text("Global\nvariant\ncount", globalPlotX2, globalPlotY1);
}

void drawVariantCounts(int[][][] counters){
    Cell[][][] table = data.table;
    //drawing cells
    for (int i = 0; i < table.length; i++) {
        for (int j = 0; j < table.length; j++) {
            for (int k = 0; k < table.length; k++) {
                int count = counters[i][j][k];
                if(count >0){
                    // if only more than one count
                    Cell cell = table[i][j][k];
                    Rectangle rec = cell.rect;
                    //rectangle
                    noStroke();
                    fill(colorSelectionTrans);
                    rectMode(CORNER);
                    rect(rec.x, rec.y, rec.width, rec.height);

                    fill(colorWhite);
                    textAlign(RIGHT, TOP);
                    textSize(12);
                    text("variant:"+count, rec.x+rec.width, rec.y);
                    // fill(color_cyan_trans);
                    // textAlign(LEFT, TOP);
                    // textSize(20);
                    // text(count, rec.x, rec.y);
                }


            }
        }
    }
}

void drawTextBtns(){
    //export VCF
    textSize(12);
    textAlign(LEFT, TOP);

    fill((text_btn_mouseover[0]?colorCyan: colorText));
    text(export_label, text_btn_rects[0].x, text_btn_rects[1].y);

    //migrating mode
    fill((text_btn_mouseover[1]?colorCyan: colorText));
    text((_MIGRATING?migrating_label[0]:migrating_label[1]), text_btn_rects[1].x, text_btn_rects[1].y);

    //optimise buttion
    fill(text_btn_mouseover[2]?colorCyan: colorText);
    text(optimisation_label, text_btn_rects[2].x, text_btn_rects[2].y);


}



















