class Cell{
	int[] indexes;
	HashSet<Variant> variants;
	HashMap<Integer, HashMap> maps;   //<SampleIndex, Hashmap>

	Rectangle rect; //display rectangle
    int maxCoverage = 200;
    int minCoverage = 1;
    int increment = 5;
    int counterSize = maxCoverage / increment;
    int counterMinIndex = 0;
    int counterMaxIndex = counterSize - 1;

    boolean isSelected = false;

    //constructor
	Cell(int i , int j, int k){
		variants = new HashSet<Variant>();
		indexes = new int[3];
		indexes[0] = i;
		indexes[1] = j;
		indexes[2] = k;
		maps = new HashMap<Integer, HashMap>(); //sampleIndex, HashMap
	}

	//add variant to the cell, and organized in hashmap
	void addVariant(Variant v) {
        if (!variants.contains(v)) {
            variants.add(v);
            for (int i = 0; i < v.rows.size(); i++) {
                Row r = v.rows.get(i);
                int sampleIndex = r.sampleIndex;
                int coverage = r.coverage;
                Sample sample = data.samples[sampleIndex];
                int currentCoverage = sample.currentCoverage;
                if (coverage >= currentCoverage) {
                    //coverage, array
                    HashMap<Integer, HashSet<Row>> rowArray = maps.get(sampleIndex);
                    if (rowArray == null) {
                        rowArray = new HashMap<Integer, HashSet<Row>>();
                        maps.put(sampleIndex, rowArray);
                    }
                    HashSet<Row> rows = (HashSet<Row>) rowArray.get(coverage);
                    if (rows == null) {
                        rows = new HashSet<Row>();
                        rowArray.put(coverage, rows);
                    }
                    rows.add(r);
                }else{
                    //coverage is below currentCoverage
                }
            }
            //assign cell's index
            // println("debug: old"+Arrays.toString(v.cellIndex)+" new:"+Arrays.toString(indexes));
            v.cellIndex = indexes;
        }else{
            println("Error:Cell:addVariant(): cell alreay contains the variant");
        }
    }

    //remove all the rows assoicated with the variant
    void removeVariant(Variant v) {
        if(variants.contains(v)){
            variants.remove(v);
            ArrayList<Row> rows = v.rows;
            for (int i = 0; i < rows.size(); i++) {
                Row r = rows.get(i);
                int coverage = r.coverage;
                int sampleIndex = r.sampleIndex;
                HashMap hm = maps.get(sampleIndex);
                if(hm == null){
                    Sample sample = data.samples[sampleIndex];
                    int currentCoverage = sample.currentCoverage;
                    if(coverage <= currentCoverage){
                        // println("Cell.removeVariant(): the row was not added when moved previously: coverage="+coverage+" cCoverage="+currentCoverage);
                    }else{
                        // println("Error:Cell.removeVariant():hm is null: sampleIndex ="+ sampleIndex+"  cellindex="+Arrays.toString(indexes)+  "coverage="+coverage+" cCoverage="+currentCoverage);      
                    }
                }else{
                    HashSet<Row> rowList = (HashSet<Row>) hm.get(coverage);
                    if(rowList == null){
                        // println("Error:Cell.removeVariant(): rowList is null.  sampleIndex ="+ sampleIndex+"  cellindex="+Arrays.toString(indexes)+"coverage="+coverage);
                    }else if (rowList.contains(r)) {
                        //remove
                        rowList.remove(r);
                    }else{
                        // println("Error:Cell.removeVariant(): rowList does not contain the row");
                    }             
                }
            }
        }else{
            println(v.variantID+" is not in the cell:this cell="+Arrays.toString(indexes)+"  v="+Arrays.toString(v.cellIndex));        
        }
    }

    //get histograms
    int[][] getCountArrays() {
        //returning array
        int[][] result = new int[3][counterSize];
        //sample order
        int[] sampleOrder = _sampleOrder;
        //per sample
        for (int i = 0; i < _sampleOrder.length; i++) {
            int sampleIndex = sampleOrder[i];
            HashMap map = maps.get(sampleIndex); // <coverage, ArrayList>
            if (map == null) {
                //no data for this sample in this cell   
            } else {
                //iterate through
                for (Iterator it = map.entrySet().iterator(); it.hasNext();) {
                    Map.Entry entry = (Map.Entry)it.next();
                    int coverage = (Integer) entry.getKey();
                    int index = coverageToCounterIndex(coverage);
                    HashSet<Row> rows = (HashSet<Row>)entry.getValue();
                    result[i][index] += rows.size();
                }
            }
        }
        return result;
    }

    int coverageToCounterIndex(int coverage) {
        int index = round(constrain(map(coverage, minCoverage, maxCoverage, counterMinIndex, counterMaxIndex), counterMinIndex, counterMaxIndex));
        return index;
    }

}
