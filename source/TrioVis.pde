//Test migrating
import java.awt.Rectangle;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import processing.pdf.*;

import java.util.concurrent.*;


int _STAGE_WIDTH = 1200;
int _STAGE_HEIGHT = 780;
PFont _FONT;

String _PROGRESS_MESSSAGE = "";

DataSet data;
PredictorThread predictor;


boolean saveFrame = false;
boolean fileChosen = false;
boolean isLoading = true;
boolean isWritingFile = false;
boolean isUpdating = false;// while variants are migrating

int updateCounter = 0;

boolean _MIGRATING = true;


//********* color ************ //
int colorBackground = 0xFFEDEDED;
int color_cyan_trans = color(0, 174, 237, 120);
int color_transparent = color(255, 255, 255, 0);
int color_lightgray_trans = color(80, 80, 80, 120);
int[] colorSampleArray = {0xFF66C2A5, 0xFFFC8D62, 0xFF8DA0CB};
int[] colorTransSampleArray = {0x7D66C2A5, 0x7DFC8D62, 0x7D8DA0CB};
int colorText = 0xFF666666;
int colorLightGray = 0xFFE6E6E6;
int colorDarkGray = 0xFF999999;
int colorGrayTrans = 0x7DCCCCCC;
int colorSelectionTrans = 0x66009CCE;//00698c;
int colorWhite = 0xFFFFFFFF;
int colorGray = 0xFFCCCCCC;
int colorPossible = 0xFFFFFFFF;
int colorImpossible = 0xFFCCCCCC;
int colorCyan = color(0, 174, 237);

void setup(){
	size(_STAGE_WIDTH, _STAGE_HEIGHT);
	_FONT = createFont("Monospaced", 10);
	textFont(_FONT);
	smooth();

    // selectInput("Select a parent VCF file:", "parentOne");

    
	//MODEL class
	data = new DataSet();
    //setup controller before starting the thread
    setupController();
	// Runnable chooseFile  = new FileLoaderThread();
	// new Thread(chooseFile).start();

    // updator = new UpdateThread(1000, "variant_updator");
    if(isTestMode){
        fileChosen = true;
        startLoadingFiles();
    }
}

void parentOne(File selection){
    if(selection == null){
        println("file was not selected");

    }else{
        println("selected file is:"+selection.getAbsolutePath());
        _files[0] = selection;
    }
}
void parentTwo(File selection){
     if(selection == null){
        println("file was not selected");
    }else{
        println("selected file is:"+selection.getAbsolutePath());
        _files[1] = selection;
    }
}

void child(File selection){
     if(selection == null){
        println("file was not selected");
    }else{
        println("selected file is:"+selection.getAbsolutePath());
        _files[2] = selection;
    }
}

void startLoadingFiles(){
    Runnable chooseFile  = new FileLoaderThread();
    new Thread(chooseFile).start();
}

//called once the data is loaded
void initialize(){
	println("--- initialize()");
    println("debug:total number of variants = "+data.allVariants.size());
    String sampleData = "";
	for(int i = 0; i<data.samples.length; i++){
        Sample s = data.samples[i];
        println("debug:"+s.sampleID+": total row count ="+ s.rows.size());
        sampleData += s.sampleID+":"+s.rows.size()+" variants\n";
    }
    sampleData+="total number of variants:"+data.allVariants.size();
    _PROGRESS_MESSSAGE = "Processing the data...";
    data.setupZygosity(); //check zygosity of each Row per sample
    //setup table
    _PROGRESS_MESSSAGE = "Setting up the table... \n"+sampleData;
    data.setupTable();

    //display
    _PROGRESS_MESSSAGE = "Setting up the display...";
    println("Trio.setupCellCoordinates()");
    setupCellCoordinates();

    //make distribution
    _PROGRESS_MESSSAGE = "Setting up the distribution...";
    data.calculateVarfreqDistributions();
    data.calculateCoverageDistributions();

    //predictor thread
    _PROGRESS_MESSSAGE ="Setting up the predictor histogram...";
    if(predictor == null){
        predictor = new PredictorThread();
        new Thread(predictor).start();
    }

    isLoading = false;
}

void debug(){
    //print cell map
    println("----cell[2][0][0]");
    Cell cell = data.table[2][0][0];
    HashMap<Integer, HashMap> maps = cell.maps;
    Iterator ite = maps.entrySet().iterator();
    while(ite.hasNext()){
        Map.Entry entry = (Map.Entry) ite.next();
        int sampleIndex = (Integer)(entry.getKey());
        HashMap table = (HashMap<Integer, ArrayList<Row>>) entry.getValue();
        ArrayList<Integer> coverages = new ArrayList<Integer>();
        coverages.addAll(table.keySet());
        println("sample:"+_sampleLabels[sampleIndex]);
        println("coverage:"+coverages);
    }
}

void draw(){
	if (saveFrame) {
        println("start of saving");
        beginRecord(PDF, "Plot-####.pdf");
    }

    background(colorBackground);
// file loading page
    if(!fileChosen){
        // println("file not chosen");
        //Instruction
        fill(colorText);
        textAlign(LEFT, TOP);
        textSize(14f);
        text("Trio analysis: Please select 3 VCF files:", plotX1, plotY1);

        //label
        textAlign(RIGHT, CENTER);
        fill(colorSampleArray[0]);
        text("Parent 1:", label_x_r, (float)path_rects[0].getCenterY());
        fill(colorSampleArray[1]);
        text("Parent 2:", label_x_r, (float)path_rects[1].getCenterY());
        fill(colorSampleArray[2]);
        text("Child:", label_x_r, (float)path_rects[2].getCenterY());

        //path
        //background
        rectMode(CORNER);
        fill(colorWhite);
        noStroke();
        rect(p1_path_rect.x, p1_path_rect.y, p1_path_rect.width, p1_path_rect.height);
        rect(p2_path_rect.x, p2_path_rect.y, p2_path_rect.width, p2_path_rect.height);
        rect(c_path_rect.x, c_path_rect.y, c_path_rect.width, c_path_rect.height);

        //path text
        for(int i = 0; i<_files.length; i++){
            File f = _files[i];
            if(f!= null){
                fill(colorText);
                textSize(12f);
                textAlign(LEFT, CENTER);
                text(f.getName(),path_rects[i].x+MARGIN, (float)path_rects[i].getCenterY());
            }
        }

        //draw button
        //rectangle
        noStroke();

        if(p1_btn.contains(mouseX, mouseY)){
            fill(colorCyan);
        }else{
            fill(colorText);
        }
        rect(p1_btn.x, p1_btn.y, p1_btn.width, p1_btn.height);
        
        if(p2_btn.contains(mouseX, mouseY)){
            fill(colorCyan);
        }else{
            fill(colorText);
        }
        rect(p2_btn.x, p2_btn.y, p2_btn.width, p2_btn.height);
        
        if(c_btn.contains(mouseX, mouseY)){
            fill(colorCyan);
        }else{
            fill(colorText);
        }
        rect(c_btn.x, c_btn.y, c_btn.width, c_btn.height);
        
        if(go_btn.contains(mouseX, mouseY)){
            fill(colorCyan);
        }else{
            fill(colorText);
        }
        rect(go_btn.x, go_btn.y, go_btn.width, go_btn.height);

        //button text
        fill(colorWhite);
        textAlign(CENTER, CENTER);
        textSize(12f);
        text("select", (float)(p1_btn.getCenterX()), (float)(p1_btn.getCenterY()));
        text("select", (float)(p2_btn.getCenterX()), (float)(p2_btn.getCenterY()));
        text("select", (float)(c_btn.getCenterX()), (float)(c_btn.getCenterY()));
        text("go", (float)(go_btn.getCenterX()), (float)(go_btn.getCenterY()));       
    }else if (isLoading && !isWritingFile) { // while loading files
        stroke(160);
        noFill();
        rect(_STAGE_WIDTH / 2 - 150, _STAGE_HEIGHT / 2, 300, 10);
        fill(160);
        float w = map(currentLoaded, 0, fileSize, 0, 300);
        rect(_STAGE_WIDTH / 2 - 150, _STAGE_HEIGHT / 2, w, 10);
        textSize(14);
        textAlign(CENTER);
        fill(160);
        text(_PROGRESS_MESSSAGE, _STAGE_WIDTH / 2, _STAGE_HEIGHT / 2 + 30);
    } else if(!isLoading && isWritingFile){ // while wirting file
        drawVarfreqDistibution();
        drawCoverageDistribution();
        drawUpAndDownBtns();
        drawControllerLabels();
        drawPredictor();
        drawTableLabel();
        drawTable();
        drawGlobalCount();

        //draw white box
        rectMode(CORNERS);
        fill(colorBackground, 200);
        noStroke();
        rect(0,0,_STAGE_WIDTH, _STAGE_HEIGHT);

        //text
        fill(colorText);
        textAlign(CENTER, CENTER);
        text("Writing VCF files....", (_STAGE_WIDTH/2), (_STAGE_HEIGHT/2));

    }else { //finished loading
        // println("draw()");
        drawVarfreqDistibution();
        drawCoverageDistribution();
        drawUpAndDownBtns();
        drawControllerLabels();
        drawTable();
        drawTableLabel();
        drawGlobalCount();
        drawPredictor();
        drawTextBtns();
        
        if(isUpdating){
            // predictor.updateAll();
            //draw updating msg
            updateCounter++;
            String updateMsg = "updating";
            if(updateCounter == 0){

            }else if(updateCounter == 1){
                updateMsg +=".";
            }else if(updateCounter == 2){
                updateMsg +="..";
            }else if(updateCounter == 3){
                updateMsg +="...";
            }else{
                updateCounter = 0;
            }
            textAlign(LEFT, TOP);
            fill(colorText);
            textSize(10f);
            text(updateMsg, updateMsgX, updateMsgY);
            // println("Draw while update");
        }else{
            if(optimisationThread != null){
                if(!optimisationThread.doneOnce){
                    //show progress
                    fill(255, 255, 255, 120);
                    noStroke();
                    rect(0, 0, _STAGE_WIDTH, _STAGE_HEIGHT);
                    //loading bar
                    stroke(160);
                    noFill();
                    rectMode(CORNER);
                    rect(_STAGE_WIDTH / 2 - 150, _STAGE_HEIGHT / 2, 300, 10);
                    fill(160);
                    float w = map(optimisationThread.currentIndex, 0, optimisationThread.totalIteration, 0, 300);
                    rect(_STAGE_WIDTH / 2 - 150, _STAGE_HEIGHT / 2, w, 10);
                    textSize(14);
                    textAlign(CENTER);
                    fill(160);
                    text("Please wait awhile to optimise...", _STAGE_WIDTH / 2, _STAGE_HEIGHT / 2 + 30);
                }else{
                    noLoop();
                }
            }else{
                noLoop();    
            }

        }

    }
    
    if (saveFrame) {
        // this.control.draw();
        endRecord();
        saveFrame = false;
        println("end of saving");
    }
}
